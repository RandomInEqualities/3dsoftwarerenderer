#pragma once

#include "Model.hpp"
#include "Quaternion.hpp"
#include "Time.hpp"
#include "Types.hpp"
#include "Vector3.hpp"

#include <memory>
#include <vector>

namespace render
{

class Instruction;

/**
 * An entity is the base object that we render. It contains the geometry of the
 * entity and have basic support for moving around in the scene.
 */
class Entity
{
public:
    /**
     * Copy and move construction and assignment works as one would expect.
     */
    Entity() = default;
    Entity(const Entity& entity);
    Entity& operator=(const Entity& entity);
    Entity(Entity&& entity) = default;
    Entity& operator=(Entity&& entity) = default;

    /**
     * The triangles in the entity.
     */
    const Model& getModel() const;
    void setModel(const Model& model);

    /**
     * The entity position in the scene.
     */
    Vector3<real64> getPosition() const;
    void setPosition(const Vector3<real64>& position);

    /**
     * The entity rotation in the scene.
     */
    Quaternion getRotation() const;
    void setRotation(const Quaternion& rotation);

    /**
     * An entity can move and rotate according to given instructions. There are a move and a
     * rotation instruction queue. An instruction is derived from the Instruction class.
     *
     * This whole instruction system is not good and could f.x. be replaced by a node tree
     * system. This would be much more flexible and eliminate the need for the two separate
     * move and rotation queues.
     */
    void addMoveInstruction(std::unique_ptr<Instruction> instruction);
    void addRotateInstruction(std::unique_ptr<Instruction> instruction);
    void clearMoveInstructions();
    void clearRotationInstructions();

    /**
     * If true the entity should receive time updates, so it can process instructions.
     */
    bool isInstructionsEnabled() const;
    void setInstructionsEnabled(const bool enabled);
    void toggleInstructionsEnabled();

    /**
     * Call every frame if the entity should update its state.
     */
    void update(Seconds elapsed);

    /**
     * Make an entity rotate around itself.
     */
    static void setupRotationInLocalSpace(Entity& entity, Vector3<real64> axis, real64 speed);

private:
    Model m_model;
    Vector3<real64> m_position{0.0, 0.0, 0.0};
    Quaternion m_rotation;

    std::vector<std::unique_ptr<Instruction>> m_moveInstructions;
    size_t m_currentMove = 0;
    std::vector<std::unique_ptr<Instruction>> m_rotateInstructions;
    size_t m_currentRotation = 0;
    bool m_instructionsEnabled = false;
};

/**
 * Base instruction class for creating instructions that change entities.
 */
class Instruction
{
public:
    /**
     * Update the given entity, this is called every frame with the elapsed frame time.
     */
    virtual void updateEntity(Entity& entity, Seconds elapsed) = 0;

    /**
     * Retrieve the next instruction.
     *
     * Return index of the next instruction. Typically this is currentInstruction + 1.
     * But it is user defined in derived classes.
     */
    virtual size_t nextInstruction(size_t currentInstruction) = 0;

    /**
     * Return a new identical object located on the heap.
     */
    virtual std::unique_ptr<Instruction> clone() const = 0;

    /**
     * Make sure subclasses gets their destructors called.
     */
    virtual ~Instruction() = default;
};

/**
 * An instruction for making an entity move towards a position.
 */
class MoveInstruction final : public Instruction
{
public:
    explicit MoveInstruction(Vector3<real64> postion, real64 m_moveSpeed);
    void updateEntity(Entity& entity, Seconds elapsed) override;
    size_t nextInstruction(size_t currentInstruction) override;
    std::unique_ptr<Instruction> clone() const override;

private:
    Vector3<real64> m_position;
    real64 m_moveSpeed;
    bool m_reachedEnd;
};

/**
 * An instruction for making an entity rotation around itself.
 */
class RotateInstruction final : public Instruction
{
public:
    explicit RotateInstruction();
    explicit RotateInstruction(Quaternion rotation, real64 angularSpeed);

    void updateEntity(Entity& entity, Seconds elapsed) override;
    size_t nextInstruction(size_t currentInstruction) override;
    std::unique_ptr<Instruction> clone() const override;

private:
    Quaternion m_rotation;
    real64 m_angularSpeed;
    bool m_reachedEnd;
};

/**
 * An instruction for making an entity wait before excuting next instruction.
 */
class WaitInstruction final : public Instruction
{
public:
    explicit WaitInstruction(Seconds waitTime);

    void updateEntity(Entity& entity, Seconds elapsed) override;
    size_t nextInstruction(size_t currentInstruction) override;
    std::unique_ptr<Instruction> clone() const override;

private:
    Seconds m_remainingWaitTime;
};

/**
 * An instruction for making an entity jump to another previous instruction.
 */
class RepeatInstruction final : public Instruction
{
public:
    explicit RepeatInstruction(size_t repeatIndex);

    void updateEntity(Entity& entity, Seconds elapsed) override;
    size_t nextInstruction(size_t currentInstruction) override;
    std::unique_ptr<Instruction> clone() const override;

private:
    size_t m_repeatIndex;
};

/**
 * An instruction for making an entity stop executing instructions.
 */
class StopInstruction final : public Instruction
{
public:
    void updateEntity(Entity& entity, Seconds elapsed) override;
    size_t nextInstruction(size_t currentInstruction) override;
    std::unique_ptr<Instruction> clone() const override;
};

} // namespace render
