#pragma once

#include "Matrix3x3.hpp"
#include "Matrix4x4.hpp"
#include "Quaternion.hpp"
#include "Types.hpp"
#include "Vector2.hpp"
#include "Vector3.hpp"
#include "Vector4.hpp"

#include <iosfwd>

namespace render
{

/**
 * Debug pretty printing of vectors and matrices.
 */
std::ostream& operator<<(std::ostream& stream, const class Vector2<real64>& vector);
std::ostream& operator<<(std::ostream& stream, const class Vector3<real64>& vector);
std::ostream& operator<<(std::ostream& stream, const class Vector4<real64>& vector);
std::ostream& operator<<(std::ostream& stream, const class Quaternion& q);
std::ostream& operator<<(std::ostream& stream, const class Matrix3x3& m);
std::ostream& operator<<(std::ostream& stream, const class Matrix4x4& m);

} // namespace render
