#pragma once

#include "Assert.hpp"
#include "Math.hpp"

namespace render
{

/**
 * A 4-dimensional vector.
 */
template<typename T>
class Vector4
{
public:
    constexpr Vector4();
    constexpr Vector4(T X, T Y, T Z, T W);

    /**
     * The length of the vector.
     */
    constexpr T length() const;

    /**
     * Normalize the vector have to a length of 1.
     */
    constexpr Vector4<T>& normalize();

    /**
     * Copy the vector and normalize the copy.
     */
    constexpr Vector4<T> normalized() const;

    /**
     * Addition and substraction of other vectors.
     */
    constexpr Vector4<T> operator-() const;
    constexpr Vector4<T> operator-(const Vector4<T>& right) const;
    constexpr Vector4<T> operator+(const Vector4<T>& right) const;
    constexpr Vector4<T>& operator+=(const Vector4<T>& right);
    constexpr Vector4<T>& operator-=(const Vector4<T>& right);

    /**
     * Multiplication with other classes.
     */
    template<typename P, typename C>
    friend constexpr Vector4<P> operator*(const Vector4<P>& left, const C right);
    template<typename P, typename C>
    friend constexpr Vector4<P> operator*(const C left, const Vector4<P>& right);
    template<typename P, typename C>
    friend constexpr Vector4<P>& operator*=(Vector4<P>& left, const C right);

    /**
     * Division with other classes.
     */
    template<typename P, typename C>
    friend constexpr Vector4<P> operator/(const Vector4<P>& left, const C right);
    template<typename P, typename C>
    friend constexpr Vector4<P>& operator/=(Vector4<P>& left, const C right);

    T x{};
    T y{};
    T z{};
    T w{};
};

// Implementation Details.

template<typename T>
constexpr Vector4<T>::Vector4()
{
}

template<typename T>
constexpr Vector4<T>::Vector4(T X, T Y, T Z, T W) : x(X), y(Y), z(Z), w(W)
{
}

template<typename T>
constexpr T Vector4<T>::length() const
{
    return math::sqrt(x * x + y * y + z * z + w * w);
}

template<typename T>
constexpr Vector4<T>& Vector4<T>::normalize()
{
    const T currentLength = length();
    if (math::isZero(currentLength))
    {
        RENDER_ASSERT(false, "Can't normalize zero vector");
        return *this;
    }
    if (!math::isEqual(currentLength, 1.0))
    {
        x /= currentLength;
        y /= currentLength;
        z /= currentLength;
        w /= currentLength;
    }
    return *this;
}

template<typename T>
constexpr Vector4<T> Vector4<T>::normalized() const
{
    Vector4<T> vector = *this;
    vector.normalize();
    return vector;
}

template<typename T>
constexpr Vector4<T> Vector4<T>::operator-() const
{
    return {-x, -y, -z, -w};
}

template<typename T>
constexpr Vector4<T> Vector4<T>::operator-(const Vector4<T>& right) const
{
    return {x - right.x, y - right.y, z - right.z, w - right.w};
}

template<typename T>
constexpr Vector4<T> Vector4<T>::operator+(const Vector4<T>& right) const
{
    return {x + right.x, y + right.y, z + right.z, w + right.w};
}

template<typename T>
constexpr Vector4<T>& Vector4<T>::operator+=(const Vector4<T>& right)
{
    x += right.x;
    y += right.y;
    z += right.z;
    w += right.w;
    return *this;
}

template<typename T>
constexpr Vector4<T>& Vector4<T>::operator-=(const Vector4<T>& right)
{
    x -= right.x;
    y -= right.y;
    z -= right.z;
    w -= right.w;
    return *this;
}

template<typename P, typename C>
constexpr Vector4<P> operator*(const Vector4<P>& left, const C right)
{
    return {left.x * right, left.y * right, left.z * right, left.w * right};
}

template<typename P, typename C>
constexpr Vector4<P> operator*(const C left, const Vector4<P>& right)
{
    return {left * right.x, left * right.y, left * right.z, left * right.w};
}

template<typename P, typename C>
constexpr Vector4<P>& operator*=(Vector4<P>& left, const C right)
{
    left.x *= right;
    left.y *= right;
    left.z *= right;
    left.w *= right;
    return left;
}

template<typename P, typename C>
constexpr Vector4<P> operator/(const Vector4<P>& left, const C right)
{
    return {left.x / right, left.y / right, left.z / right, left.w / right};
}

template<typename P, typename C>
constexpr Vector4<P>& operator/=(Vector4<P>& left, const C right)
{
    left.x /= right;
    left.y /= right;
    left.z /= right;
    left.w /= right;
    return left;
}

} // namespace render
