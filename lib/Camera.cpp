#include "Camera.hpp"
#include "Assert.hpp"
#include "Math.hpp"
#include "Matrix3x3.hpp"
#include "Matrix4x4.hpp"
#include "Time.hpp"
#include "Types.hpp"
#include "Vector3.hpp"

namespace render
{

Camera::Camera()
{
    init({0.0, 0.0, 0.0}, {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0});
}

void Camera::init(const Vector3<real64>& position, const Vector3<real64>& target, const Vector3<real64>& up)
{
    RENDER_ASSERT(!math::isZero(up.length()), "Cannot use an up vector that is zero.");
    RENDER_ASSERT(!math::isZero((position - target).length()), "The direction vector must not be zero.");

    m_position = position;
    m_target = target;
    m_up = up;
    m_up.normalize();
    calculateCameraBasis();
}

Vector3<real64> Camera::getPosition() const
{
    return m_position;
}

void Camera::setPosition(const Vector3<real64>& position)
{
    init(position, m_target, m_up);
}

Vector3<real64> Camera::getTarget() const
{
    return m_position + m_cameraBasisZ;
}

void Camera::setTarget(const Vector3<real64>& target)
{
    init(m_position, target, m_up);
}

Vector3<real64> Camera::getUp() const
{
    return m_cameraBasisY;
}

void Camera::setUp(const Vector3<real64>& up)
{
    init(m_position, m_target, up);
}

Matrix4x4 Camera::getViewTransform() const
{
    // Translate from world to camera.
    Matrix4x4 translation;
    translation.setupTranslation(-m_position);

    // Rotate into the camera basis.
    Matrix3x3 rotation;
    rotation.m00 = m_cameraBasisX.x;
    rotation.m01 = m_cameraBasisY.x;
    rotation.m02 = m_cameraBasisZ.x;

    rotation.m10 = m_cameraBasisX.y;
    rotation.m11 = m_cameraBasisY.y;
    rotation.m12 = m_cameraBasisZ.y;

    rotation.m20 = m_cameraBasisX.z;
    rotation.m21 = m_cameraBasisY.z;
    rotation.m22 = m_cameraBasisZ.z;

    return multiply(rotation.computeInverse(), translation);
}

void Camera::moveForward(const Seconds elapsedTime)
{
    m_position -= elapsedTime.count() * m_moveSpeed * m_cameraBasisZ;
}

void Camera::moveBackwards(const Seconds elapsedTime)
{
    m_position += elapsedTime.count() * m_moveSpeed * m_cameraBasisZ;
}

void Camera::strafeLeft(const Seconds elapsedTime)
{
    m_position -= elapsedTime.count() * m_moveSpeed * m_cameraBasisX;
}

void Camera::strafeRight(const Seconds elapsedTime)
{
    m_position += elapsedTime.count() * m_moveSpeed * m_cameraBasisX;
}

void Camera::tiltRight(const Seconds elapsedTime)
{
    tilt(-elapsedTime);
}

void Camera::tiltLeft(const Seconds elapsedTime)
{
    tilt(elapsedTime);
}

void Camera::mouseMoved(const real64 deltaX, const real64 deltaY)
{
    // Horizontal mouse movement
    if (!math::isZero(deltaX))
    {
        const Vector3<real64> rotationAxis = -m_cameraBasisY;
        const real64 rotationAngle = m_mouseSensitivityX * 2.0 * math::pi * deltaX;

        Matrix3x3 rotation;
        rotation.setupRotation(rotationAxis, rotationAngle);

        m_cameraBasisX *= rotation;
        m_cameraBasisZ *= rotation;
    }

    // Vertical mouse movement
    if (!math::isZero(deltaY))
    {
        const Vector3<real64> rotationAxis = -m_cameraBasisX;
        const real64 rotationAngle = m_mouseSensitivityY * 2.0 * math::pi * deltaY;

        Matrix3x3 rotation;
        rotation.setupRotation(rotationAxis, rotationAngle);

        m_cameraBasisY *= rotation;
        m_cameraBasisZ *= rotation;
    }
}

real64 Camera::getMoveSpeed() const
{
    return m_moveSpeed;
}

void Camera::setMoveSpeed(const real64 MetersPerSecond)
{
    m_moveSpeed = MetersPerSecond;
}

real64 Camera::getTiltSpeed() const
{
    return m_tiltSpeed;
}

void Camera::setTiltSpeed(const real64 MetersPerSecond)
{
    m_tiltSpeed = MetersPerSecond;
}

void Camera::tilt(const Seconds elapsedTime)
{
    const real64 angle = m_tiltSpeed * elapsedTime.count();
    Matrix3x3 rotation;
    rotation.setupRotation(m_cameraBasisZ, angle);
    m_cameraBasisX *= rotation;
    m_cameraBasisY *= rotation;
}

void Camera::calculateCameraBasis()
{
    m_cameraBasisZ = m_position - m_target;
    m_cameraBasisZ.normalize();

    m_cameraBasisX = crossProduct(m_up, m_cameraBasisZ);
    m_cameraBasisX.normalize();

    m_cameraBasisY = crossProduct(m_cameraBasisZ, m_cameraBasisX);
    m_cameraBasisY.normalize();
}

} // namespace render
