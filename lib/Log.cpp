#include "Log.hpp"
#include "StringUtils.hpp"

#include <cstdlib>
#include <fstream>
#include <iostream>

namespace render::log
{

void log(const std::string& message)
{
    // Control where error messages are logged.
    static const bool LogToConsole = true;
    static const bool LogToFile = true;
    static const std::string LogFileName = "Log.txt";

    const std::string time = getTimeAndDate();
    if (LogToConsole)
    {
        std::cout << time << ". " << message << std::endl;
    }
    if (LogToFile)
    {
        std::ofstream logFile;
        logFile.open(LogFileName, std::ios::out | std::ios::app);
        logFile << time << ". " << message << std::endl;
        logFile.close();
    }
}

void logAssert(const std::string& condition, const std::string& message, const std::string& file, int line)
{
    std::string assertFailure = "Assert failure.\n";
    if (!message.empty())
    {
        assertFailure += message + ": ";
    }
    if (!condition.empty())
    {
        assertFailure += condition + "\n";
    }
    if (!file.empty())
    {
        assertFailure += "File: " + stripPath(file) + "[Line " + std::to_string(line) + "]";
    }

    log(assertFailure);
    std::abort();
}

} // namespace render::log
