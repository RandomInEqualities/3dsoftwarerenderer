#include "Renderer.hpp"
#include "Assert.hpp"
#include "Math.hpp"
#include "Vector3.hpp"
#include "Vector4.hpp"

#include <algorithm>
#include <array>
#include <cmath>
#include <cstring>
#include <utility>
#include <vector>

namespace render
{

/**
 * Triangle that goes through the rendering pipeline. The triangles that models have (the Triangle
 * class) can not have extra parameters that are nice to carry around.
 */
struct RendererTriangle
{
    // Triangle coordinates in clip space.
    Vector4<real64> p0c;
    Vector4<real64> p1c;
    Vector4<real64> p2c;

    // Parameters that are used to calculate the barycentric coordinates of a point on
    // the triangle. See Barycentric Coordinates as Interpolants for more info.
    real64 w1w2alpha0;
    real64 w2w0alpha1;
    real64 w0w1alpha2;
    real64 w1w2beta0;
    real64 w2w0beta1;
    real64 w0w1beta2;
    real64 w1w2gamma0;
    real64 w2w0gamma1;
    real64 w0w1gamma2;

    // What should be drawn.
    bool drawColours;
    bool drawWireframe;

    // The triangle colour and texture.
    Colour uniformColour;
    const Texture* texture;
    Vector2<real64> p0TexturePos;
    Vector2<real64> p1TexturePos;
    Vector2<real64> p2TexturePos;
};

Renderer::Renderer(const ResourceLoader& ResourceLoader) : m_resources(ResourceLoader)
{
    m_cameraToClip.setupIdentity();
    m_worldToCamera.setupIdentity();
    setResolution(m_resolution);
    updateCameraToClipMatrix();
}

void Renderer::setViewTransform(const Matrix4x4& worldToCamera)
{
    m_worldToCamera = worldToCamera;
}

void Renderer::setResolution(Vector2<int32> resolution)
{
    RENDER_ASSERT(resolution.x > 0, "Render resolution X must be positive.");
    RENDER_ASSERT(resolution.y > 0, "Render resolution Y must be positive.");

    m_resolution = resolution;
    m_halfResolution = resolution / 2;
    createBuffers();
}

void Renderer::setZoom(Vector2<real64> zoom)
{
    RENDER_ASSERT(!math::isZero(zoom.x), "Render zoom X must not be zero.");
    RENDER_ASSERT(!math::isZero(zoom.y), "Render zoom Y must not be zero.");

    m_zoom = zoom;
    updateCameraToClipMatrix();
}

const std::vector<uint8>& Renderer::getFrameBuffer() const
{
    return m_frameBuffer;
}

bool Renderer::isDrawingColours() const
{
    return m_drawColours;
}

void Renderer::setDrawColours(bool enabled)
{
    m_drawColours = enabled;
}

bool Renderer::isDrawingWireframes() const
{
    return m_drawWireframes;
}

void Renderer::setDrawWireframes(bool enabled)
{
    m_drawWireframes = enabled;
}

void Renderer::render(const std::vector<Entity>& entities)
{
    clearDrawBuffers();

    // Transform and draw the triangles in the entity models.
    const Matrix4x4 worldToClip = m_cameraToClip * m_worldToCamera;
    for (const Entity& entity : entities)
    {
        Matrix4x4 entityToWorld;
        entityToWorld.setupLocalToParent(entity.getPosition(), entity.getRotation());
        const Matrix4x4 entityToClip = worldToClip * entityToWorld;

        for (const ModelTriangle& modelTriangle : entity.getModel().triangles)
        {
            RendererTriangle triangle;
            initializeTriangle(triangle, modelTriangle);

            // Project each vertex into clip space.
            triangle.p0c = entityToClip * modelTriangle.p0;
            triangle.p1c = entityToClip * modelTriangle.p1;
            triangle.p2c = entityToClip * modelTriangle.p2;

            // Specify how to draw the triangle.
            triangle.drawColours = m_drawColours;
            triangle.drawWireframe = m_drawWireframes;

            renderTriangle(triangle);
        }
    }
}

void Renderer::updateCameraToClipMatrix()
{
    // This sets up a perspective camera to clip projection matrix. The clip matrix does three things:
    // - Put the projection divisor into w. Usually perspective projection is division by z, now
    //   you should divide by w (w is equal to z if the clip matrix does perspective projection).
    // - Scale x and y by the horizontal and vertical zoom. x -> x*m_zoom.x, y -> y*m_zoom.y.
    // - Normalize the z coordinate, such that the near clip plane is at z = -w, and far clip
    //   plane at z = w.
    m_cameraToClip.setupIdentity();
    m_cameraToClip.m00 = m_zoom.x;
    m_cameraToClip.m11 = m_zoom.y;
    m_cameraToClip.m22 = (m_farZ + m_nearZ) / (m_nearZ - m_farZ);
    m_cameraToClip.m32 = -1.0;
    m_cameraToClip.tz = 2 * m_farZ * m_nearZ / (m_nearZ - m_farZ);
    m_cameraToClip.tw = 0.0;
}

void Renderer::createBuffers()
{
    const int32 pixelCount = m_resolution.x * m_resolution.y;
    m_depthBuffer.resize(pixelCount);
    m_defaultDepthBuffer.resize(pixelCount);

    const int32 frameBufferBytes = 4 * pixelCount;
    m_frameBuffer.resize(frameBufferBytes);
    m_defaultFrameBuffer.resize(frameBufferBytes);

    clearBuffers();
}

void Renderer::clearBuffers()
{
    for (size_t index = 0; index < m_depthBuffer.size(); index++)
    {
        m_defaultDepthBuffer[index] = math::highest<real64>();
    }
    for (size_t index = 0; index < m_frameBuffer.size(); index += 4)
    {
        m_defaultFrameBuffer[index + 0] = m_backgroundColour.getRed();
        m_defaultFrameBuffer[index + 1] = m_backgroundColour.getGreen();
        m_defaultFrameBuffer[index + 2] = m_backgroundColour.getBlue();
        m_defaultFrameBuffer[index + 3] = m_backgroundColour.getAlpha();
    }
    clearDrawBuffers();
}

void Renderer::clearDrawBuffers()
{
    // We only have the default buffers so we can use memcpy to clear the real draw buffers here.
    // Looping over draw buffers is suprisingly expensive. E.g. 1920x1080 buffer is a 2 million pixel loop.
    std::memcpy(m_depthBuffer.data(), m_defaultDepthBuffer.data(), m_depthBuffer.size() * sizeof(real64));
    std::memcpy(m_frameBuffer.data(), m_defaultFrameBuffer.data(), m_frameBuffer.size() * sizeof(uint8));
}

void Renderer::renderTriangle(RendererTriangle& tri)
{
    // Don't draw colours on back-facing triangles.
    if (!m_drawBackFacingTriangles && tri.drawColours)
    {
        // Compute the clip space triangle normal.
        const Vector3<real64> p0{tri.p0c.x, tri.p0c.y, tri.p0c.w};
        const Vector3<real64> p0p1{tri.p1c.x - tri.p0c.x, tri.p1c.y - tri.p0c.y, tri.p1c.w - tri.p0c.w};
        const Vector3<real64> p0p2{tri.p2c.x - tri.p0c.x, tri.p2c.y - tri.p0c.y, tri.p2c.w - tri.p0c.w};
        const Vector3<real64> normal = crossProduct(p0p1, p0p2);

        if (dotProduct(p0, normal) < 0)
        {
            tri.drawColours = false;
        }
    }

    if (!tri.drawColours && !tri.drawWireframe)
    {
        return;
    }

    if (clipWithFrustrum(tri))
    {
        drawTriangle(tri);
    }
}

void Renderer::initializeTriangle(RendererTriangle& triangle, const ModelTriangle& from)
{
    triangle.uniformColour = from.colour;
    triangle.texture = nullptr;
    if (from.textureId.IsValid())
    {
        const Texture& texture = m_resources.getTexture(from.textureId);
        triangle.texture = &texture;

        triangle.p0TexturePos.x = texture.width * from.p0TexturePos.x;
        triangle.p0TexturePos.y = texture.height * from.p0TexturePos.y;
        triangle.p1TexturePos.x = texture.width * from.p1TexturePos.x;
        triangle.p1TexturePos.y = texture.height * from.p1TexturePos.y;
        triangle.p2TexturePos.x = texture.width * from.p2TexturePos.x;
        triangle.p2TexturePos.y = texture.height * from.p2TexturePos.y;
    }
}

void Renderer::initializeTriangle(RendererTriangle& triangle, const RendererTriangle& from)
{
    triangle.drawColours = from.drawColours;
    triangle.drawWireframe = from.drawWireframe;
    triangle.uniformColour = from.uniformColour;
    triangle.texture = from.texture;
}

bool Renderer::clipWithFrustrum(const RendererTriangle& tri)
{
    // An arbitrary point p is inside the view frustum if:
    //      -p.w < p.x < p.w
    //      -p.w < p.y < p.w
    //      -p.w < p.z < p.w
    // This is true because of how x and y are scaled to the screen and because the clip matrix
    // scales z. We use this to determine what triangles to draw, clip and discard.
    const bool p0cOutNear = tri.p0c.z < -tri.p0c.w;
    const bool p0cOutFar = tri.p0c.w < tri.p0c.z;
    const bool p0cOutLeft = tri.p0c.x < -tri.p0c.w;
    const bool p0cOutRight = tri.p0c.w < tri.p0c.x;
    const bool p0cOutBot = tri.p0c.y < -tri.p0c.w;
    const bool p0cOutTop = tri.p0c.w < tri.p0c.y;

    const bool p1cOutNear = tri.p1c.z < -tri.p1c.w;
    const bool p1cOutFar = tri.p1c.w < tri.p1c.z;
    const bool p1cOutLeft = tri.p1c.x < -tri.p1c.w;
    const bool p1cOutRight = tri.p1c.w < tri.p1c.x;
    const bool p1cOutBot = tri.p1c.y < -tri.p1c.w;
    const bool p1cOutTop = tri.p1c.w < tri.p1c.y;

    const bool p2cOutNear = tri.p2c.z < -tri.p2c.w;
    const bool p2cOutFar = tri.p2c.w < tri.p2c.z;
    const bool p2cOutLeft = tri.p2c.x < -tri.p2c.w;
    const bool p2cOutRight = tri.p2c.w < tri.p2c.x;
    const bool p2cOutBot = tri.p2c.y < -tri.p2c.w;
    const bool p2cOutTop = tri.p2c.w < tri.p2c.y;

    // Any triangle that are completely outside the left, right, bot, top, near or far frustum
    // planes are discarded.
    if (p0cOutNear && p1cOutNear && p2cOutNear)
    {
        return false;
    }
    if (p0cOutFar && p1cOutFar && p2cOutFar)
    {
        return false;
    }
    if (p0cOutLeft && p1cOutLeft && p2cOutLeft)
    {
        return false;
    }
    if (p0cOutRight && p1cOutRight && p2cOutRight)
    {
        return false;
    }
    if (p0cOutBot && p1cOutBot && p2cOutBot)
    {
        return false;
    }
    if (p0cOutTop && p1cOutTop && p2cOutTop)
    {
        return false;
    }

    // Clip triangles that are partly inside the near clip plane.
    if (p0cOutNear || p1cOutNear || p2cOutNear)
    {
        clipWithPlane(tri, ClipPlane::Near, p0cOutNear, p1cOutNear, p2cOutNear);
        return false;
    }

    // We could clip triangles that are inside the far, left, right, bottom and top clip planes,
    // but we do not gain much from it and rounding errors can result in lines between triangles.
    return true;
}

void Renderer::clipWithPlane(
    const RendererTriangle& tri, const Renderer::ClipPlane plane, bool p0cOutside, bool p1cOutside, bool p2cOutside)
{
    Vector4<real64> p0 = tri.p0c;
    Vector4<real64> p1 = tri.p1c;
    Vector4<real64> p2 = tri.p2c;
    Vector2<real64> p0TexturePos = tri.p0TexturePos;
    Vector2<real64> p1TexturePos = tri.p1TexturePos;
    Vector2<real64> p2TexturePos = tri.p2TexturePos;

    // When we clip a triangle with a plane if vertices is on one side of the plane and two
    // vertices are on the other side. We order p0, p1 and p2 such that p0 is the 'lonely' one.
    bool invertedNormal = false;
    if ((p0cOutside && p2cOutside) || (!p0cOutside && !p2cOutside))
    {
        std::swap(p0, p1);
        std::swap(p0TexturePos, p1TexturePos);
        std::swap(p0cOutside, p1cOutside);
        invertedNormal = true;
    }
    else if ((p0cOutside && p1cOutside) || (!p0cOutside && !p1cOutside))
    {
        std::swap(p0, p2);
        std::swap(p0TexturePos, p2TexturePos);
        std::swap(p0cOutside, p2cOutside);
        invertedNormal = true;
    }

    // Find the intersections between the clip plane and p0p1 and p0p2. Utilises that we have either
    // x = w, x = -w, y = w, y = -w, z = w or z = -w at the clip planes. For example the near clip
    // plane has z = -w. This makes it possible to interpolate the intersections.
    real64 t1 = 0;
    real64 t2 = 0;
    switch (plane)
    {
    case ClipPlane::Near:
        t1 = (p0.z + p0.w) / (p0.z + p0.w - p1.z - p1.w);
        t2 = (p0.z + p0.w) / (p0.z + p0.w - p2.z - p2.w);
        break;
    case ClipPlane::Far:
        t1 = (p0.z - p0.w) / (p0.z - p0.w - p1.z + p1.w);
        t2 = (p0.z - p0.w) / (p0.z - p0.w - p2.z + p2.w);
        break;
    case ClipPlane::Left:
        t1 = (p0.x + p0.w) / (p0.x + p0.w - p1.x - p1.w);
        t2 = (p0.x + p0.w) / (p0.x + p0.w - p2.x - p2.w);
        break;
    case ClipPlane::right:
        t1 = (p0.x - p0.w) / (p0.x - p0.w - p1.x + p1.w);
        t2 = (p0.x - p0.w) / (p0.x - p0.w - p2.x + p2.w);
        break;
    case ClipPlane::Bottom:
        t1 = (p0.y + p0.w) / (p0.y + p0.w - p1.y - p1.w);
        t2 = (p0.y + p0.w) / (p0.y + p0.w - p2.y - p2.w);
        break;
    case ClipPlane::Top:
        t1 = (p0.y - p0.w) / (p0.y - p0.w - p1.y + p1.w);
        t2 = (p0.y - p0.w) / (p0.y - p0.w - p2.y + p2.w);
        break;
    }
    Vector4<real64> p0p1intersection = p0 + (p1 - p0) * t1;
    Vector4<real64> p0p2intersection = p0 + (p2 - p0) * t2;

    // Due to rounding errors it can happen that the intersections are outside the view frustum.
    // If this happens we get infinite recursion.
    switch (plane)
    {
    case ClipPlane::Near:
        p0p1intersection.z = -p0p1intersection.w;
        p0p2intersection.z = -p0p2intersection.w;
        break;
    case ClipPlane::Far:
        p0p1intersection.z = p0p1intersection.w;
        p0p2intersection.z = p0p2intersection.w;
        break;
    case ClipPlane::Left:
        p0p1intersection.x = -p0p1intersection.w;
        p0p2intersection.x = -p0p2intersection.w;
        break;
    case ClipPlane::right:
        p0p1intersection.x = p0p1intersection.w;
        p0p2intersection.x = p0p2intersection.w;
        break;
    case ClipPlane::Bottom:
        p0p1intersection.y = -p0p1intersection.w;
        p0p2intersection.y = -p0p2intersection.w;
        break;
    case ClipPlane::Top:
        p0p1intersection.y = p0p1intersection.w;
        p0p2intersection.y = p0p2intersection.w;
        break;
    }

    if (!p0cOutside)
    {
        // The lonely point is within the view frustum and we need one new triangle. Construct it
        // from p0 and the intersections.
        RendererTriangle newTriangle;
        initializeTriangle(newTriangle, tri);
        newTriangle.p0c = p0;
        newTriangle.p1c = p0p1intersection;
        newTriangle.p2c = p0p2intersection;
        newTriangle.p0TexturePos = p0TexturePos;
        newTriangle.p1TexturePos = p0TexturePos + (p1TexturePos - p0TexturePos) * t1;
        newTriangle.p2TexturePos = p0TexturePos + (p2TexturePos - p0TexturePos) * t2;
        if (invertedNormal)
        {
            std::swap(newTriangle.p0c, newTriangle.p1c);
            std::swap(newTriangle.p0TexturePos, newTriangle.p1TexturePos);
        }
        renderTriangle(newTriangle);
    }
    else
    {
        // The lonely point is outside the view frustum and we need two new triangles. Construct
        // them from p1, p2 and the intersections.
        RendererTriangle newTriangle1;
        initializeTriangle(newTriangle1, tri);
        newTriangle1.p0c = p0p1intersection;
        newTriangle1.p1c = p1;
        newTriangle1.p2c = p2;
        newTriangle1.p0TexturePos = p0TexturePos + (p1TexturePos - p0TexturePos) * t1;
        newTriangle1.p1TexturePos = p1TexturePos;
        newTriangle1.p2TexturePos = p2TexturePos;
        if (invertedNormal)
        {
            std::swap(newTriangle1.p0c, newTriangle1.p1c);
            std::swap(newTriangle1.p0TexturePos, newTriangle1.p1TexturePos);
        }
        renderTriangle(newTriangle1);

        RendererTriangle newTriangle2;
        initializeTriangle(newTriangle2, tri);
        newTriangle2.p0c = p0p2intersection;
        newTriangle2.p1c = p0p1intersection;
        newTriangle2.p2c = p2;
        newTriangle2.p0TexturePos = p0TexturePos + (p2TexturePos - p0TexturePos) * t2;
        newTriangle2.p1TexturePos = p0TexturePos + (p1TexturePos - p0TexturePos) * t1;
        newTriangle2.p2TexturePos = p2TexturePos;
        if (invertedNormal)
        {
            std::swap(newTriangle2.p0c, newTriangle2.p1c);
            std::swap(newTriangle2.p0TexturePos, newTriangle2.p1TexturePos);
        }
        renderTriangle(newTriangle2);
    }
}

void Renderer::drawTriangle(RendererTriangle& tri)
{
    // In this function we partition the triangle into horizontal lines and draw them from left
    // to right. The actual drawing is done by the ProcessScanLine function, this function tells
    // which lines that ProcessScanLine should draw.

    // The triangle coordinates in screen space.
    Vector2<int32> p0s;
    Vector2<int32> p1s;
    Vector2<int32> p2s;
    p0s.x = static_cast<int32>((1.0 + tri.p0c.x / tri.p0c.w) * m_halfResolution.x);
    p0s.y = static_cast<int32>((1.0 - tri.p0c.y / tri.p0c.w) * m_halfResolution.y);
    p1s.x = static_cast<int32>((1.0 + tri.p1c.x / tri.p1c.w) * m_halfResolution.x);
    p1s.y = static_cast<int32>((1.0 - tri.p1c.y / tri.p1c.w) * m_halfResolution.y);
    p2s.x = static_cast<int32>((1.0 + tri.p2c.x / tri.p2c.w) * m_halfResolution.x);
    p2s.y = static_cast<int32>((1.0 - tri.p2c.y / tri.p2c.w) * m_halfResolution.y);

    // Draw the triangle outline and exit if that is all we needed to draw.
    if (tri.drawWireframe)
    {
        drawLine(p0s, p1s, m_wireframeColour);
        drawLine(p0s, p2s, m_wireframeColour);
        drawLine(p1s, p2s, m_wireframeColour);
    }
    if (!tri.drawColours)
    {
        return;
    }

    // Calculate the barycentric parameters that are constant across the triangle. We pass
    // these to the ProcessScanLine functions such that they don't need to recalculate them.
    // See Barycentric Coordinates as Interpolants for a complete explanation.
    const real64 w0w1 = tri.p0c.w * tri.p1c.w;
    const real64 w2w0 = tri.p0c.w * tri.p2c.w;
    const real64 w1w2 = tri.p1c.w * tri.p2c.w;
    tri.w1w2alpha0 = w1w2 * (p1s.y - p2s.y);
    tri.w2w0alpha1 = w2w0 * (p2s.y - p0s.y);
    tri.w0w1alpha2 = w0w1 * (p0s.y - p1s.y);
    tri.w1w2beta0 = w1w2 * (p2s.x - p1s.x);
    tri.w2w0beta1 = w2w0 * (p0s.x - p2s.x);
    tri.w0w1beta2 = w0w1 * (p1s.x - p0s.x);
    tri.w1w2gamma0 = w1w2 * (p1s.x * p2s.y - p2s.x * p1s.y);
    tri.w2w0gamma1 = w2w0 * (p2s.x * p0s.y - p0s.x * p2s.y);
    tri.w0w1gamma2 = w0w1 * (p0s.x * p1s.y - p1s.x * p0s.y);

    // Order the points such that p0s.y < p1s.y < p2s.y. The barycentric parameters must be
    // calculated before we do this.
    if (p2s.y < p1s.y)
    {
        std::swap(p2s, p1s);
    }
    if (p2s.y < p0s.y)
    {
        std::swap(p2s, p0s);
    }
    if (p1s.y < p0s.y)
    {
        std::swap(p1s, p0s);
    }

    // Only draw the line part inside the screen.
    int32 yMin = p0s.y;
    int32 yMax = p2s.y;
    if (yMin < 0)
    {
        yMin = 0;
    }
    if (m_resolution.y < yMax)
    {
        yMax = m_resolution.y;
    }

    // If the line is outside the screen we are done.
    if (yMax < 0 || m_resolution.y <= yMin || yMax <= yMin)
    {
        return;
    }

    // Draw each horizontal line in the triangle, left to right, for each y value. With the way
    // that the triangle is ordered we draw from p0p2 to p0p1 and p1p2 or we draw from p0p1 and
    // p1p2 to p0p2 (make a drawing:).
    if ((p1s.x - p0s.x) * (p2s.y - p0s.y) < (p2s.x - p0s.x) * (p1s.y - p0s.y))
    {
        for (int32 y = yMin; y < yMax; y++)
        {
            if (y < p1s.y)
            {
                processScanLine(y, p0s, p1s, p0s, p2s, tri);
            }
            else
            {
                processScanLine(y, p1s, p2s, p0s, p2s, tri);
            }
        }
    }
    else
    {
        for (int32 y = yMin; y < yMax; y++)
        {
            if (y < p1s.y)
            {
                processScanLine(y, p0s, p2s, p0s, p1s, tri);
            }
            else
            {
                processScanLine(y, p0s, p2s, p1s, p2s, tri);
            }
        }
    }
}

void Renderer::processScanLine(const int32 y, const Vector2<int32> a, const Vector2<int32> b, const Vector2<int32> c,
    const Vector2<int32> d, const RendererTriangle& tri)
{
    // At the given y value, this function draws a horizontal line from ab to cd.

    // Find the minimum and maximum x value to draw between.
    const real64 gradientAB = (a.y == b.y) ? 1.0 : static_cast<real64>(y - a.y) / static_cast<real64>(b.y - a.y);
    const real64 gradientCD = (c.y == d.y) ? 1.0 : static_cast<real64>(y - c.y) / static_cast<real64>(d.y - c.y);
    int32 xMin = static_cast<int32>(a.x + (b.x - a.x) * gradientAB);
    int32 xMax = static_cast<int32>(c.x + (d.x - c.x) * gradientCD);

    // Only draw the part inside the screen.
    if (xMin < 0)
    {
        xMin = 0;
    }
    if (m_resolution.x < xMax)
    {
        xMax = m_resolution.x;
    }

    // If the line is outside the screen we are done.
    if (xMax < xMin || m_resolution.x <= xMin || xMax < 0)
    {
        return;
    }

    // As in Barycentric Coordinates as Interpolants we calculate the initial areas of the triangle.
    // A0 is the area spanned by back-projecting (x,y), p1s and p2s back to eye space and then
    // finding the triangle area, the formula has been reduced to yield very few computational
    // steps. A1 and A2 are respectively the area spanned by the back-projection of (x,y), p0s, p2s
    // and (x,y), p0s, p1s.
    real64 A0 = tri.w1w2alpha0 * xMin + tri.w1w2beta0 * y + tri.w1w2gamma0;
    real64 A1 = tri.w2w0alpha1 * xMin + tri.w2w0beta1 * y + tri.w2w0gamma1;
    real64 A2 = tri.w0w1alpha2 * xMin + tri.w0w1beta2 * y + tri.w0w1gamma2;

    for (int32 x = xMin; x < xMax; x++)
    {
        // Find the barycentric coordinates of this point. Uses the calculated eye space triangle
        // areas. We can use these coordinates to interpolate any vertex level property.
        const real64 total = A0 + A1 + A2;
        const real64 b0 = A0 / total;
        const real64 b1 = A1 / total;
        const real64 b2 = 1.0 - b0 - b1;

        // Find the depth of this point, used to determine visibility.
        const real64 w = b0 * tri.p0c.w + b1 * tri.p1c.w + b2 * tri.p2c.w;

        // Draw the point into the frame buffer.
        drawPoint(x, y, w, b0, b1, b2, tri);

        // Update the areas to be the areas at x+1. Updating like this is faster than
        // recalculating for each x.
        A0 += tri.w1w2alpha0;
        A1 += tri.w2w0alpha1;
        A2 += tri.w0w1alpha2;
    }
}

void Renderer::drawPoint(const int32 x, const int32 y, const real64 w, const real64 b0, const real64 b1,
    const real64 b2, const RendererTriangle& tri)
{
    // Check if the point is visible.
    const int32 depthIndex = x + y * m_resolution.x;
    if (m_depthBuffer[depthIndex] < w)
    {
        return;
    }
    m_depthBuffer[depthIndex] = w;

    // Determine the colour of the point and put it into the frame buffer.
    const int32 frameIndex = 4 * depthIndex;
    if (tri.texture != nullptr)
    {
        const int32 width = tri.texture->width;
        const int32 height = tri.texture->height;
        const uint8* data = tri.texture->rgba.data();

        const Vector2<real64> position = b0 * tri.p0TexturePos + b1 * tri.p1TexturePos + b2 * tri.p2TexturePos;
        const int32 column = std::clamp(static_cast<int32>(position.x), 0, width - 1);
        const int32 row = std::clamp(static_cast<int32>(position.y), 0, height - 1);

        putPixel(frameIndex, data + 4 * (column + row * width));
    }
    else
    {
        const uint8* data = tri.uniformColour.getRgba().data();
        putPixel(frameIndex, data);
    }
}

void Renderer::drawLine(const Vector2<int32> a, const Vector2<int32> b, const Colour colour)
{
    // Draw a line into the frame buffer. Assumes that this line need to be displayed and
    // gives the points maximum depth in the depth buffer.

    // Iterate over x or y depending on which gives the most smooth line.
    if (std::abs(a.y - b.y) < std::abs(a.x - b.x))
    {
        // Iterate over x and find y by y = c + s*x.
        const real64 slope = static_cast<real64>(b.y - a.y) / static_cast<real64>(b.x - a.x);
        const real64 constant = a.y - slope * a.x;

        int32 xMin;
        int32 xMax;
        if (b.x < a.x)
        {
            xMin = b.x;
            xMax = a.x;
        }
        else
        {
            xMin = a.x;
            xMax = b.x;
        }

        if (xMin < 0)
        {
            xMin = 0;
        }
        if (m_resolution.x < xMax)
        {
            xMax = m_resolution.x;
        }

        for (int32 x = xMin; x < xMax; x++)
        {
            const int32 y = static_cast<int32>(constant + slope * x);
            if (y < 0 || m_resolution.y <= y)
            {
                continue;
            }
            const int32 index = x + y * m_resolution.x;
            m_depthBuffer[index] = math::lowest<real64>();
            putPixel(4 * index, colour.getRgba().data());
        }
    }
    else
    {
        // Iterate over y and find x by x = c + s*y.
        const real64 slope = static_cast<real64>(b.x - a.x) / static_cast<real64>(b.y - a.y);
        const real64 constant = a.x - slope * a.y;

        int32 yMin;
        int32 yMax;
        if (b.y < a.y)
        {
            yMin = b.y;
            yMax = a.y;
        }
        else
        {
            yMin = a.y;
            yMax = b.y;
        }

        if (yMin < 0)
        {
            yMin = 0;
        }
        if (m_resolution.y < yMax)
        {
            yMax = m_resolution.y;
        }

        for (int32 y = yMin; y < yMax; y++)
        {
            const int32 x = static_cast<int32>(constant + slope * y);
            if (x < 0 || m_resolution.x <= x)
            {
                continue;
            }
            const int32 index = x + y * m_resolution.x;
            m_depthBuffer[index] = math::lowest<real64>();
            putPixel(4 * index, colour.getRgba().data());
        }
    }
}

void Renderer::putPixel(const int32 frameBufferIndex, const uint8* colour)
{
    std::memcpy(m_frameBuffer.data() + frameBufferIndex, colour, 4);
}

} // namespace render
