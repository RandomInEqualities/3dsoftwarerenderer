#pragma once

#include "Types.hpp"

#include <array>

namespace render
{

/**
 * A simple RGBA color.
 */
class Colour
{
public:
    constexpr explicit Colour() : rgba{0, 0, 0, 0}
    {
    }

    constexpr explicit Colour(const uint8 red, const uint8 green, const uint8 blue, const uint8 alpha)
        : rgba{red, green, blue, alpha}
    {
    }

    constexpr uint8 getRed() const
    {
        return rgba[0];
    }

    constexpr void setRed(const uint8 red)
    {
        rgba[0] = red;
    }

    constexpr uint8 getGreen() const
    {
        return rgba[1];
    }

    constexpr void setGreen(const uint8 green)
    {
        rgba[1] = green;
    }

    constexpr uint8 getBlue() const
    {
        return rgba[2];
    }

    constexpr void setBlue(const uint8 blue)
    {
        rgba[2] = blue;
    }

    constexpr uint8 getAlpha() const
    {
        return rgba[3];
    }

    constexpr void setAlpha(const uint8 alpha)
    {
        rgba[3] = alpha;
    }

    constexpr const std::array<uint8, 4>& getRgba() const
    {
        return rgba;
    }

private:
    std::array<uint8, 4> rgba;
};

} // namespace render
