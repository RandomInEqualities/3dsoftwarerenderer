#pragma once

#include "Colour.hpp"
#include "Types.hpp"
#include "Vector2.hpp"
#include "Vector3.hpp"

#include <array>
#include <string>
#include <vector>

namespace render
{

/**
 * Id for a texture.
 */
class TextureId
{
public:
    explicit TextureId()
    {
    }

    explicit TextureId(const size_t Index)
    {
        Id = static_cast<int64>(Index);
    }

    bool IsValid() const
    {
        return Id >= 0;
    }

    size_t GetIndex() const
    {
        return static_cast<size_t>(Id);
    }

private:
    int64 Id = -1;
};

/**
 * An RGBA texture image.
 */
struct Texture
{
    TextureId Id;
    int32 width = 0;
    int32 height = 0;
    std::vector<uint8> rgba;
};

} // namespace render
