#include "LogMath.hpp"

#include <iomanip>
#include <ostream>

namespace render
{

std::ostream& operator<<(std::ostream& stream, const Vector2<real64>& vector)
{
    stream << vector.x << " " << vector.y;
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const Vector3<real64>& vector)
{
    stream << vector.x << " " << vector.y << " " << vector.z;
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const Vector4<real64>& vector)
{
    stream << vector.x << " " << vector.y << " " << vector.z << " " << vector.w;
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const Quaternion& q)
{
    stream << q.w << " " << q.x << " " << q.y << " " << q.z;
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const Matrix3x3& m)
{
    const auto flags = stream.flags();
    const auto precision = stream.precision();
    stream.precision(3);
    stream.setf(std::ios::fixed);
    stream.setf(std::ios::right);

    using std::setw;
    const std::streamsize w = 10;
    stream << "\n"
           << setw(w) << m.m00 << setw(w) << m.m01 << setw(w) << m.m02 << "\n"
           << setw(w) << m.m10 << setw(w) << m.m11 << setw(w) << m.m12 << "\n"
           << setw(w) << m.m20 << setw(w) << m.m21 << setw(w) << m.m22 << "\n";

    stream.precision(precision);
    stream.flags(flags);
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const Matrix4x4& m)
{
    const auto flags = stream.flags();
    const auto precision = stream.precision();
    stream.precision(3);
    stream.setf(std::ios::fixed);
    stream.setf(std::ios::right);

    using std::setw;
    const std::streamsize w = 10;
    stream << "\n"
           << setw(w) << m.m00 << setw(w) << m.m01 << setw(w) << m.m02 << setw(w) << m.tx << "\n"
           << setw(w) << m.m10 << setw(w) << m.m11 << setw(w) << m.m12 << setw(w) << m.ty << "\n"
           << setw(w) << m.m20 << setw(w) << m.m21 << setw(w) << m.m22 << setw(w) << m.tz << "\n"
           << setw(w) << m.m30 << setw(w) << m.m31 << setw(w) << m.m32 << setw(w) << m.tw << "\n";

    stream.precision(precision);
    stream.flags(flags);
    return stream;
}

} // namespace render
