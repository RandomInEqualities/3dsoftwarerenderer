#pragma once

#include "Log.hpp"

#ifndef NDEBUG
#define RENDER_ASSERT(condition, message)                                    \
    do                                                                       \
    {                                                                        \
        if (!(condition))                                                    \
        {                                                                    \
            render::log::logAssert(#condition, message, __FILE__, __LINE__); \
        }                                                                    \
    } while (false)
#else
#define RENDER_ASSERT(condition, message) ((void)0)
#endif
