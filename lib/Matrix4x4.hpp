#pragma once

#include "Quaternion.hpp"
#include "Types.hpp"
#include "Vector3.hpp"
#include "Vector4.hpp"

namespace render
{

/**
 * This Matrix4x4 class is the brain of the math environment. It represent all currently
 * implemented mathematical transformations.
 *
 * But if it is 4x4 we can not multiply it with 3D vectors? Yes and no. The 3D vectors are
 * also assumed to have an extra 4D element so [x y z] -> [x y z w]. This extra element w is
 * almost always equal to 1.
 *
 * The grand reason for using a 4x4 matrix is because translation of points is a non-linear
 * operation, so it can not be represented by a 3x3 matrix. Thus we expand the 3D room to a
 * 4D room and get translation contained in matrix multiplication. We are shearing 4D space
 * to get translation in 3D space.
 *
 * So we are working with vectors and matrices, where:
 *
 *             [ m11 m12 m13 tx ][ x ]       [ m11*x + m12*y + m13*z + tx*w ]
 *             [ m21 m22 m23 ty ][ y ]   =   [ m21*x + m22*y + m23*z + ty*w ]
 *             [ m31 m32 m33 tz ][ z ]   =   [ m31*x + m32*y + m33*z + tz*w ]
 *             [ m41 m42 m43 tw ][ w ]       [ m41*x + m42*y + m43*z + tw*w ]
 *
 * Almost all the time we have m41=m42=m43=0 and tw=w=1, such that:
 *
 *             [ m11 m12 m13 tx ][ x ]       [ m11*x + m12*y + m13*z + tx ]
 *             [ m21 m22 m23 ty ][ y ]   =   [ m21*x + m22*y + m23*z + ty ]
 *             [ m31 m32 m33 tz ][ z ]   =   [ m31*x + m32*y + m33*z + tz ]
 *             [  0   0   0   1 ][ 1 ]       [             1              ]
 *
 * This is similar to a 3x3 matrix except for the last column. The first 3x3 elements
 * (m11,m12,m13,m21,m22,m23,m31,m32,m33) are used to represent linear transformations i.e scale,
 * projection, rotation, shearing and reflection. These elements are exactly the same as for their
 * 3D counterpart. The last 3 elements (tx, ty, tz) are used to represent translation.
 *
 * Reference and awesome book about 3D math:
 *     3D Math Primer for Graphics and Game Development, by Ian Parberry and Fletcher Dunn.
 */
class Matrix4x4
{
public:
    /**
     * Compute the determinant of the matrix.
     */
    real64 computeDeterminant() const;

    /**
     * Compute the inverse of the matrix.
     */
    Matrix4x4 computeInverse() const;

    /**
     * Setup the identity matrix.
     */
    Matrix4x4& setupIdentity();

    /**
     * Setup a translation matrix.
     */
    Matrix4x4& setupTranslation(const Vector3<real64>& t);

    /**
     * Setup a rotation matrix.
     */
    Matrix4x4& setupRotation(Vector3<real64> axis, real64 radians);
    Matrix4x4& setupRotation(const Quaternion& q);

    /**
     * Setup scale along an axis.
     */
    Matrix4x4& setupScale(Vector3<real64> axis, real64 scale);

    /**
     * Setup local to parent transformation. We assume that the position and rotation is the
     * local's position and rotation in parent space.
     */
    Matrix4x4& setupLocalToParent(const Vector3<real64>& position, const Quaternion& rotation);

    /**
     * Setup parent to local transformation. We assume that the position and rotation is the
     * local's position and rotation in parent space.
     */
    Matrix4x4& setupParentToLocal(const Vector3<real64>& position, const Quaternion& rotation);

    /**
     * The 16 matrix elements.
     */
    real64 m00 = 0.0, m01 = 0.0, m02 = 0.0, tx = 0.0;
    real64 m10 = 0.0, m11 = 0.0, m12 = 0.0, ty = 0.0;
    real64 m20 = 0.0, m21 = 0.0, m22 = 0.0, tz = 0.0;
    real64 m30 = 0.0, m31 = 0.0, m32 = 0.0, tw = 0.0;
};

/**
 * Matrix4x4-Matrix4x4 multiplication.
 */
Matrix4x4 operator*(const Matrix4x4& left, const Matrix4x4& right);
Matrix4x4& operator*=(Matrix4x4& left, const Matrix4x4& right);

/**
 * Matrix4x4-Vector4 multiplication.
 */
Vector4<real64> operator*(const Matrix4x4& left, const Vector4<real64>& right);
Vector4<real64>& operator*=(Vector4<real64>& left, const Matrix4x4& right);

/**
 * Matrix4x4-Vector3 multiplication. Assumes the 3D vector has a fourth coordinate that is =1.
 */
Vector4<real64> operator*(const Matrix4x4& left, const Vector3<real64>& right);

} // namespace render
