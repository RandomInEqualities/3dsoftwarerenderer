#pragma once

#include "Colour.hpp"
#include "Texture.hpp"
#include "Types.hpp"
#include "Vector2.hpp"
#include "Vector3.hpp"

#include <vector>

namespace render
{

/**
 * A single triangle in a model.
 */
struct ModelTriangle
{
    /**
     * The three vertices of the triangle.
     */
    Vector3<real64> p0;
    Vector3<real64> p1;
    Vector3<real64> p2;

    /**
     * The id of the triangle texture (is invalid if it has no texture).
     */
    TextureId textureId;

    /**
     * The vertex positions on the texture. The x and y values are between 0.0 and 1.0.
     */
    Vector2<real64> p0TexturePos;
    Vector2<real64> p1TexturePos;
    Vector2<real64> p2TexturePos;

    /**
     * A uniform colour. This can be used to colour the triangle if it does not have a texture.
     */
    Colour colour;
};

/**
 * A model is a collection of triangles that can be rendered.
 */
struct Model
{
    std::vector<ModelTriangle> triangles;
};

} // namespace render
