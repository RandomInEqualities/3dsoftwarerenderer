#include "ResourceLoader.hpp"
#include "TgaImage.hpp"
#include "Types.hpp"
#include "WavefrontObjModel.hpp"

#include <random>
#include <stdexcept>

namespace render
{

Model ResourceLoader::loadObjModel(const std::string& filename)
{
    // Read the obj file into an obj::Model. Throws runtime_error on failure.
    format::wavefront::ObjModel objModel;
    objModel.readFromFile(filename);

    // Random colour generation for the triangle colours.
    std::random_device seed_device;
    std::default_random_engine generator(seed_device());
    std::uniform_int_distribution<uint16> uniform(0, 255);

    // Convert the obj model into triangles and textures.
    Model model;
    for (const format::wavefront::ObjFace& objFace : objModel.getFaces())
    {
        if (objFace.vertices.size() != 3)
        {
            throw std::runtime_error("We only support triangle faces at the moment.");
        }

        ModelTriangle triangle;
        triangle.p0 = objFace.vertices[0];
        triangle.p1 = objFace.vertices[1];
        triangle.p2 = objFace.vertices[2];

        if (objFace.textures.size() == 3)
        {
            triangle.p0TexturePos = objFace.textures[0];
            triangle.p1TexturePos = objFace.textures[1];
            triangle.p2TexturePos = objFace.textures[2];
        }

        if (objFace.material.empty())
        {
            // give the triangle a random colour.
            triangle.colour.setRed(static_cast<uint8>(uniform(generator)));
            triangle.colour.setGreen(static_cast<uint8>(uniform(generator)));
            triangle.colour.setBlue(static_cast<uint8>(uniform(generator)));
            triangle.colour.setAlpha(255);
        }
        else
        {
            // give the triangle the diffuse material colour.
            const format::wavefront::ObjMaterial& material = objModel.getMaterial(objFace.material);
            triangle.colour.setRed(static_cast<uint8>(255.0 * material.diffuse.x));
            triangle.colour.setGreen(static_cast<uint8>(255.0 * material.diffuse.y));
            triangle.colour.setBlue(static_cast<uint8>(255.0 * material.diffuse.z));
            triangle.colour.setAlpha(static_cast<uint8>(255.0 * material.transparency));

            // load the material texture (if any).
            if (!material.diffuseFilename.empty())
            {
                triangle.textureId = loadTgaTexture(material.diffuseFilename);
            }
        }
        model.triangles.emplace_back(triangle);
    }
    return model;
}

TextureId ResourceLoader::loadTgaTexture(const std::string& filename)
{
    if (auto foundTexture = m_textureNames.find(filename); foundTexture != m_textureNames.end())
    {
        return foundTexture->second;
    }

    // Load the texture.
    Texture texture;
    uint32 width = 0;
    uint32 height = 0;
    texture.rgba = format::tga::loadImage(filename, width, height);
    texture.width = static_cast<int32>(width);
    texture.height = static_cast<int32>(height);
    texture.Id = TextureId{m_textures.size()};
    m_textures.emplace_back(texture);
    m_textureNames[filename] = texture.Id;
    return texture.Id;
}

const Texture& ResourceLoader::getTexture(const TextureId id) const
{
    return m_textures.at(id.GetIndex());
}

} // namespace render
