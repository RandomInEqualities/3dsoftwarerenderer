#pragma once

#include "Types.hpp"
#include "Vector2.hpp"
#include "Vector3.hpp"

#include <string>
#include <vector>

namespace render::format::wavefront
{

/**
 * Geometry for a single face in an obj model.
 */
struct ObjFace
{
    /**
     * The vertex positions. There can be an arbitrary amount of vertices.
     */
    std::vector<Vector3<real64>> vertices;

    /**
     * Optional vertex normals. The size is either zero or vertices.size().
     */
    std::vector<Vector3<real64>> normals;

    /**
     * Optional texture positions. The size is either zero or vertices.size().
     * The x and y values are between 0.0 and 1.0.
     */
    std::vector<Vector2<real64>> textures;

    /**
     * Optional group and material.
     */
    std::string group;
    std::string material;
};

/**
 * Data for a single material in an obj model.
 */
struct ObjMaterial
{
    /**
     * The material name. Faces reference this name.
     */
    std::string name;

    /**
     * Material parameters. The ambient, diffuse, specular and transparency are between 0.0
     * and 1.0. The illumination specifies the desired illumination mode.
     */
    Vector3<real64> ambient{0.0, 0.0, 0.0};
    Vector3<real64> diffuse{0.0, 0.0, 0.0};
    Vector3<real64> specular{0.0, 0.0, 0.0};
    real64 transparency = 1.0;
    int32 illumination = 0;

    /**
     * Optional textures. We hold a filename to the textures.
     */
    std::string ambientFilename;
    std::string diffuseFilename;
    std::string specularFilename;
};

/**
 * Model geometry and data for a wavefront .obj file.
 *
 * OBJ files hold geometry in ASCII format. They have the obj extension. Material template library
 * files (mtl extension) are used to hold textures and Phong light model parameters. The
 * combination of OBJ files and Material files gives data for a full 3D model that can be rendered.
 *
 * This provides a subset of the obj file specification, see http://paulbourke.net/dataformats/obj/
 * for a full specification. Right now we look for vertices (v), faces (f), texture coordinates
 * (vt), normals (vn), materials (usemtl, mtllib), group names (g) and object names (o). When
 * mtllib is detected we load the the material file. In the material file we look for
 * transparency (tr, d), ambient (ka, map_ka), diffuse (kd, map_kd), specular (ks, map_ks),
 * illumination mode (illum) and of course new materials (newmtl).
 */
class ObjModel
{
public:
    /**
     * Load the model from an obj file. Throws runtime_error on failure.
     */
    void readFromFile(const std::string& filename);

    /**
     * Get an obj material from its name. Throws out_of_range on failure.
     */
    const ObjMaterial& getMaterial(const std::string& material) const;

    /**
     * Get the name of the model.
     */
    const std::string& getName() const;

    /**
     * Get the faces of the model. This is the models geometry.
     */
    const std::vector<ObjFace>& getFaces() const;

private:
    void addMaterial(const ObjMaterial& material);
    void resetMaterial(ObjMaterial& material);
    void readMaterialFile(const std::string& filename);

    std::string m_name;
    std::vector<ObjFace> m_faces;
    std::vector<ObjMaterial> m_materials;
};

} // namespace render::format::wavefront
