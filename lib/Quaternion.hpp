#pragma once

#include "Types.hpp"
#include "Vector3.hpp"

namespace render
{

class Quaternion
{
public:
    /**
     * Set up the identity quaternion.
     */
    explicit Quaternion();

    /**
     * Set up a rotation from an axis and angle.
     */
    explicit Quaternion(const Vector3<real64>& axis, real64 radians);

    /**
     * The rotation angle in radians.
     */
    real64 getAngle() const;

    /**
     * The normalized rotation axis.
     */
    Vector3<real64> getAxis() const;

    /**
     * The inverse quaternion. Makes the quaternion rotate in the opposite direction.
     */
    Quaternion computeInverse() const;

    /**
     * Quaternion exponentiation. With exponentiation we can take a fraction of a quaternion.
     * For example a fourth of the quaternion rotation can be retrieved with exponent=0.25.
     */
    Quaternion computeExponentation(real64 exponent) const;

    /**
     * Set the quaternion to the identity (1, (0,0,0)).
     */
    Quaternion& setupIdentity();

    /**
     * Construct the quaternion from an angle and a rotation axis.
     */
    Quaternion& setupRotation(Vector3<real64> axis, real64 radians);
    Quaternion& setupRotationAroundXAxis(real64 radians);
    Quaternion& setupRotationAroundYAxis(real64 radians);
    Quaternion& setupRotationAroundZAxis(real64 radians);

    /**
     * Four members w,x,y,z. If a rotation phi is around a normalized axis r=(n,m,o)
     * then w = cos(phi/2) and (x,y,z) = sin(phi/2)*r.
     */
    real64 w = 0.0;
    real64 x = 0.0;
    real64 y = 0.0;
    real64 z = 0.0;
};

/**
 * Multiplication represent the quaternion cross product. The cross product can be used to combine
 * rotation quaternions together.
 */
Quaternion operator*(const Quaternion& left, const Quaternion& right);
Quaternion& operator*=(Quaternion& left, const Quaternion& right);

/**
 * Quaternion dot product. The dotproduct is similar to a vectors dotproduct.
 */
real64 dotProduct(const Quaternion& left, const Quaternion& right);

/**
 * Spherical Linear Interpolation. Returns a quaternion that has a rotation, which is between
 * quaternion a and b's rotation. The resulting rotation is the fraction t from a to b. For
 * example with t=0.25 the returned quaternion is a fourth of the way between a and b. The
 * parameter t is assumed to be between 0.0 and 1.0.
 */
Quaternion slerp(Quaternion a, Quaternion b, real64 t);

} // namespace render
