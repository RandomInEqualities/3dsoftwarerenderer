#pragma once

#include "Types.hpp"

#include <string>
#include <vector>

namespace render::format::tga
{

/**
 * Load TGA image into an RGBA array. Throws runtime_error on failure.
 */
std::vector<uint8> loadImage(const std::string& filename, uint32& width, uint32& height);

/**
 * Save RGBA array as a TGA image. Throws std::runtime_error on failure.
 */
void saveImage(const std::string& filename, const std::vector<uint8>& image, uint32 width, uint32 height);

} // namespace render::format::tga
