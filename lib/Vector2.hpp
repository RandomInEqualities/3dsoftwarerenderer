#pragma once

#include "Assert.hpp"
#include "Math.hpp"

namespace render
{

/**
 * A 2-dimensional vector.
 */
template<typename T>
class Vector2
{
public:
    constexpr Vector2();
    constexpr Vector2(const T X, const T Y);

    /**
     * The length of the vector.
     */
    constexpr T length() const;

    /**
     * Normalize the vector have to a length of 1.
     */
    constexpr Vector2<T>& normalize();

    /**
     * Copy the vector and normalize the copy.
     */
    constexpr Vector2<T> normalized() const;

    /**
     * Addition and substraction of other vectors.
     */
    constexpr Vector2<T> operator-() const;
    constexpr Vector2<T> operator-(const Vector2<T>& right) const;
    constexpr Vector2<T> operator+(const Vector2<T>& right) const;
    constexpr Vector2<T>& operator+=(const Vector2<T>& right);
    constexpr Vector2<T>& operator-=(const Vector2<T>& right);

    /**
     * Multiplication with other vectors.
     */
    template<typename P, typename C>
    friend constexpr Vector2<P> operator*(const Vector2<P>& left, const C right);
    template<typename P, typename C>
    friend constexpr Vector2<P> operator*(const C left, const Vector2<P>& right);
    template<typename P, typename C>
    friend constexpr Vector2<P>& operator*=(Vector2<P>& left, const C right);

    /**
     * Division with other vectors.
     */
    template<typename P, typename C>
    friend constexpr Vector2<P> operator/(const Vector2<P>& left, const C right);
    template<typename P, typename C>
    friend constexpr Vector2<P>& operator/=(Vector2<P>& left, const C right);

    T x{};
    T y{};
};

template<typename T>
constexpr Vector2<T>::Vector2()
{
}

template<typename T>
constexpr Vector2<T>::Vector2(const T X, const T Y) : x(X), y(Y)
{
}

template<typename T>
constexpr T Vector2<T>::length() const
{
    return math::sqrt(x * x + y * y);
}

template<typename T>
constexpr Vector2<T>& Vector2<T>::normalize()
{
    const T currentLength = length();
    if (math::isZero(currentLength))
    {
        RENDER_ASSERT(false, "Can't normalize zero vector");
        return *this;
    }
    if (!math::isEqual(currentLength, 1.0))
    {
        x /= currentLength;
        y /= currentLength;
    }
    return *this;
}

template<typename T>
constexpr Vector2<T> Vector2<T>::normalized() const
{
    Vector2<T> vector = *this;
    vector.normalize();
    return vector;
}

template<typename T>
constexpr Vector2<T> Vector2<T>::operator-() const
{
    return {-x, -y};
}

template<typename T>
constexpr Vector2<T> Vector2<T>::operator-(const Vector2<T>& right) const
{
    return {x - right.x, y - right.y};
}

template<typename T>
constexpr Vector2<T> Vector2<T>::operator+(const Vector2<T>& right) const
{
    return {x + right.x, y + right.y};
}

template<typename T>
constexpr Vector2<T>& Vector2<T>::operator+=(const Vector2<T>& right)
{
    x += right.x;
    y += right.y;
    return *this;
}

template<typename T>
constexpr Vector2<T>& Vector2<T>::operator-=(const Vector2<T>& right)
{
    x -= right.x;
    y -= right.y;
    return *this;
}

template<typename P, typename C>
constexpr Vector2<P> operator*(const Vector2<P>& left, const C right)
{
    return {left.x * right, left.y * right};
}

template<typename P, typename C>
constexpr Vector2<P> operator*(const C left, const Vector2<P>& right)
{
    return {left * right.x, left * right.y};
}

template<typename P, typename C>
constexpr Vector2<P>& operator*=(Vector2<P>& left, const C right)
{
    left.x *= right;
    left.y *= right;
    return left;
}

template<typename P, typename C>
constexpr Vector2<P> operator/(const Vector2<P>& left, const C right)
{
    return Vector2<P>{left.x / right, left.y / right};
}

template<typename P, typename C>
constexpr Vector2<P>& operator/=(Vector2<P>& left, const C right)
{
    left.x /= right;
    left.y /= right;
    return left;
}

} // namespace render
