#include "Quaternion.hpp"
#include "Math.hpp"

#include <cmath>

namespace render
{

Quaternion::Quaternion() : w(1.0), x(0.0), y(0.0), z(0.0)
{
}

Quaternion::Quaternion(const Vector3<real64>& axis, const real64 radians)
{
    setupRotation(axis, radians);
}

real64 Quaternion::getAngle() const
{
    return 2 * math::acos(w);
}

Vector3<real64> Quaternion::getAxis() const
{
    // Find the rotation angle.
    const real64 angle = 2.0 * math::acos(w);
    const real64 sinAngleOver2 = math::sin(angle / 2.0);

    // If sinAngleOver2 is zero then we have no rotation. We return an arbitrary rotation axis.
    if (math::isZero(sinAngleOver2))
    {
        return {1.0, 0.0, 0.0};
    }

    return {x / sinAngleOver2, y / sinAngleOver2, z / sinAngleOver2};
}

Quaternion Quaternion::computeInverse() const
{
    Quaternion result;
    result.w = w;
    result.x = -x;
    result.y = -y;
    result.z = -z;
    return result;
}

Quaternion Quaternion::computeExponentation(const real64 exponent) const
{
    // We make sure that w != 1 so we do not have sin(alpha)=0. If we do have sin(alpha)=0 then
    // we just return this quaternion as we have no Real angular displacement anyway.
    if (!math::isEqual(std::abs(w), 1.0))
    {

        // If the quaternion q is equal to [cos(alpha) sin(alpha)*n], then it holds that
        // q^t = exp(t*log(q)) = [cos(alpha*t) sin(alpha*t)*n].
        Quaternion result;

        const real64 alpha = math::acos(w);
        result.w = math::cos(alpha * exponent);

        const real64 sinAlphaExponentOverSinAlpha = math::sin(alpha * exponent) / math::sin(alpha);

        result.x = sinAlphaExponentOverSinAlpha * x;
        result.y = sinAlphaExponentOverSinAlpha * y;
        result.z = sinAlphaExponentOverSinAlpha * z;

        return result;
    }
    else
    {
        return *this;
    }
}

Quaternion& Quaternion::setupIdentity()
{
    w = 1.0;
    x = y = z = 0.0;
    return *this;
}

Quaternion& Quaternion::setupRotation(Vector3<real64> axis, const real64 radians)
{
    axis.normalize();

    // Assign the rotation axis and angle.
    const real64 sinAngleOver2 = math::sin(radians / 2.0);
    x = sinAngleOver2 * axis.x;
    y = sinAngleOver2 * axis.y;
    z = sinAngleOver2 * axis.z;
    w = math::cos(radians / 2.0);
    return *this;
}

Quaternion& Quaternion::setupRotationAroundXAxis(const real64 radians)
{
    w = math::cos(radians / 2.0);
    x = math::sin(radians / 2.0);
    y = 0.0;
    z = 0.0;
    return *this;
}

Quaternion& Quaternion::setupRotationAroundYAxis(const real64 radians)
{
    w = math::cos(radians / 2.0);
    x = 0.0;
    y = math::sin(radians / 2.0);
    z = 0.0;
    return *this;
}

Quaternion& Quaternion::setupRotationAroundZAxis(const real64 radians)
{
    w = math::cos(radians / 2.0);
    x = 0.0;
    y = 0.0;
    z = math::sin(radians / 2.0);
    return *this;
}

Quaternion operator*(const Quaternion& a, const Quaternion& b)
{
    Quaternion result;
    result.w = a.w * b.w - a.x * b.x - a.y * b.y - a.z * b.z;
    result.x = a.w * b.x + a.x * b.w + a.y * b.z - a.z * b.y;
    result.y = a.w * b.y + a.y * b.w + a.z * b.x - a.x * b.z;
    result.z = a.w * b.z + a.z * b.w + a.x * b.y - a.y * b.x;
    return result;
}

Quaternion& operator*=(Quaternion& a, const Quaternion& b)
{
    a = b * a;
    return a;
}

real64 dotProduct(const Quaternion& a, const Quaternion& b)
{
    return a.w * b.w + a.x * b.x + a.y * b.y + a.z * b.z;
}

Quaternion slerp(Quaternion a, Quaternion b, const real64 t)
{
    RENDER_ASSERT((-1.0 < t) && (t < 1.0), "t out of range");

    // We find the angle omega between the two quaternions. For this we use that cos(omega)
    // is dotProduct(a,b). If this is negative we switch sign on a, which ensures consistent
    // results.
    real64 cosOmega = dotProduct(a, b);
    if (cosOmega < 0.0)
    {
        a.x = -a.x;
        a.y = -a.y;
        a.z = -a.z;
        a.w = -a.w;
        cosOmega = -cosOmega;
    }

    // We express the wanted quaternion in the basis of a and b. This gives a nice expression
    // where result = k0*a + k1*b.
    real64 k0;
    real64 k1;
    if (cosOmega > (1.0 - math::zeroTolerance))
    {
        // We use Linear Interpolation here result = a + t*(b-a).
        k0 = 1.0 - t;
        k1 = t;
    }
    else
    {
        // We use Spherical Linear Interpolation.
        const real64 omega = math::acos(cosOmega);
        const real64 oneOverSinOmega = 1.0 / math::sin(omega);
        k0 = oneOverSinOmega * math::sin(omega * (1.0 - t));
        k1 = oneOverSinOmega * math::sin(omega * t);
    }

    Quaternion result;
    result.w = k0 * a.w + k1 * b.w;
    result.x = k0 * a.x + k1 * b.x;
    result.y = k0 * a.y + k1 * b.y;
    result.z = k0 * a.z + k1 * b.z;
    return result;
}

} // namespace render
