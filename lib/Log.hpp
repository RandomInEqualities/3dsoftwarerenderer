#pragma once

#include "Types.hpp"

#include <string>

namespace render::log
{

/**
 * Log a message. Adds the time and date to the message.
 */
void log(const std::string& message);

/**
 * Log an assertation failure and abort the program. Logs condition, message, file and line.
 */
void logAssert(const std::string& condition, const std::string& message, const std::string& file, int line);

} // namespace render::log
