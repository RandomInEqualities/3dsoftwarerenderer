#pragma once

#include "Colour.hpp"
#include "Entity.hpp"
#include "Matrix4x4.hpp"
#include "Model.hpp"
#include "ResourceLoader.hpp"
#include "Types.hpp"
#include "Vector2.hpp"

#include <vector>

namespace render
{

struct RendererTriangle;

/**
 * Renderer renders an image of the scene into an RGBA array.
 *
 * The renderer needs to know the entities to draw (and their textures if they have any), a view
 * transform from world space to camera space, output resolution and zoom. When it have these it
 * will simply output an RGBA image of the scene taken from the camera's perspective.
 */
class Renderer
{
public:
    /**
     * The renderer requires a resource loader to lookup textures.
     */
    explicit Renderer(const ResourceLoader&);

    /**
     * Set the view transform for the camera.
     *
     * This is the position and rotation of the camera that we render from.
     */
    void setViewTransform(const Matrix4x4& worldToCamera);

    /**
     * Set the render output resolution.
     */
    void setResolution(Vector2<int32> resolution);

    /**
     * Set the zoom to use for rendering. If we should zoom in.
     */
    void setZoom(Vector2<real64> zoom);

    /**
     * Render entities in the scene into the renderers frame buffer.
     */
    void render(const std::vector<Entity>& entities);

    /**
     * Get the image that we have rendered.
     *
     * The frame buffer is the size of an RGBA image in the renderers resolution.
     */
    const std::vector<uint8>& getFrameBuffer() const;

    /**
     * Set if we should draw colours on triangles.
     */
    bool isDrawingColours() const;
    void setDrawColours(bool enabled);

    /**
     * Set if we should draw wireframes for triangles.
     */
    bool isDrawingWireframes() const;
    void setDrawWireframes(bool enabled);

private:
    enum class ClipPlane
    {
        Near,
        Far,
        right,
        Left,
        Top,
        Bottom
    };

    /**
     * Setup the kind of camera projection that we use.
     */
    void updateCameraToClipMatrix();

    void createBuffers();
    void clearBuffers();
    void clearDrawBuffers();

    void renderTriangle(RendererTriangle&);
    void initializeTriangle(RendererTriangle& triangle, const ModelTriangle& from);
    void initializeTriangle(RendererTriangle& triangle, const RendererTriangle& from);

    /**
     * Clip triangle to view frustum. Returns false if the triangle is outside the view frustum
     * and if it should not be drawn. If the triangle is partly inside the view frustum it will
     * clip it, make new drawing calls and return false.
     */
    bool clipWithFrustrum(const RendererTriangle&);

    /**
     * Clip a triangle with a plane. Makes new drawing calls for the one or two new triangles.
     */
    void clipWithPlane(const RendererTriangle&, ClipPlane plane, bool p0cOutside, bool p1cOutside, bool p2cOutside);

    /**
     * Draw a triangle into the frameBuffer.
     */
    void drawTriangle(RendererTriangle&);

    /**
     * Draw a horizontal line into the frame buffer. Draws at the given y value and from ab to cd.
     * The four points must be sorted as desired before calling.
     */
    void processScanLine(
        int32 y, Vector2<int32> a, Vector2<int32> b, Vector2<int32> c, Vector2<int32> d, const RendererTriangle&);

    /**
     * Draw a point into the frame buffer. Interpolates the triangle texture if it has a texture.
     * b0, b1, b2 are barycentric eye space coordinates for the triangle point.
     */
    void drawPoint(int32 x, int32 y, real64 w, real64 b0, real64 b1, real64 b2, const RendererTriangle&);

    /**
     * Draw a line from a to b into the frame buffer.
     */
    void drawLine(Vector2<int32> a, Vector2<int32> b, Colour colour);

    /**
     * Put a colour into the frame buffer.
     */
    void putPixel(int32 frameBufferIndex, const uint8* colour);

    const ResourceLoader& m_resources;
    Vector2<int32> m_resolution{100, 100};
    Vector2<int32> m_halfResolution{50, 50};
    Vector2<real64> m_zoom{1.0, 1.0};
    real64 m_nearZ{0.02};
    real64 m_farZ{2000.0};
    Matrix4x4 m_cameraToClip;
    Matrix4x4 m_worldToCamera;

    bool m_drawBackFacingTriangles = true;
    bool m_drawColours = true;
    bool m_drawWireframes = false;
    Colour m_wireframeColour{255, 255, 255, 255};
    Colour m_backgroundColour{151, 139, 179, 255};

    // The depth and frame buffers that we render into.
    std::vector<real64> m_depthBuffer;
    std::vector<uint8> m_frameBuffer;

    // The depth and frame buffers that we reset to every frame.
    std::vector<real64> m_defaultDepthBuffer;
    std::vector<uint8> m_defaultFrameBuffer;
};

} // namespace render
