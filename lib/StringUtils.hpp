#pragma once

#include <string>
#include <vector>

namespace render
{

/**
 * Make use of the s string literal, such that we can write "C++ style string!"s.
 */
using namespace std::literals::string_literals;

/**
 * Convert an ASCII string to lower or upper case.
 */
std::string toLower(const std::string& str);
std::string toUpper(const std::string& str);

/**
 * Extract the filename from a file path with '/' or '\' folder separation.
 */
std::string stripPath(const std::string& filePath);

/**
 * Get the current time and date.
 */
std::string getTimeAndDate(char separator = ' ');

/**
 * Split a string into words.
 */
std::vector<std::string> splitIntoWords(const std::string& str);

} // namespace render
