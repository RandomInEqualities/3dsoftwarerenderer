#pragma once

#include "Matrix4x4.hpp"
#include "Quaternion.hpp"
#include "Types.hpp"
#include "Vector3.hpp"

namespace render
{

/**
 * A 3-by-3 matrix.
 */
class Matrix3x3
{
public:
    /**
     * Compute the determinant of the matrix.
     */
    real64 computeDeterminant() const;

    /**
     * Compute the inverse of the matrix.
     */
    Matrix3x3 computeInverse() const;

    /**
     * Setup the identity matrix.
     */
    Matrix3x3& setupIdentity();

    /**
     * Setup a rotation matrix.
     */
    Matrix3x3& setupRotation(Vector3<real64> axis, real64 radians);
    Matrix3x3& setupRotation(const Quaternion& q);

    /**
     * Setup scale along an axis.
     */
    Matrix3x3& setupScale(Vector3<real64> axis, real64 scale);

    /**
     * Convert this matrix to a 4x4 matrix.
     */
    Matrix4x4 as4x4();

    /**
     * The 12 matrix elements.
     */
    real64 m00 = 0.0, m01 = 0.0, m02 = 0.0;
    real64 m10 = 0.0, m11 = 0.0, m12 = 0.0;
    real64 m20 = 0.0, m21 = 0.0, m22 = 0.0;
};

/**
 * Matrix3x3-Matrix3x3 multiplication.
 */
Matrix3x3 operator*(const Matrix3x3& left, const Matrix3x3& right);
Matrix3x3& operator*=(Matrix3x3& left, const Matrix3x3& right);

/**
 * Matrix3x3-Vector3 multiplication.
 */
Vector3<real64> operator*(const Matrix3x3& left, const Vector3<real64>& right);
Vector3<real64>& operator*=(Vector3<real64>& left, const Matrix3x3& right);

/**
 * Matrix3x3-Matrix4x4 multiplication.
 *
 * Provided because they save a few computations compared to converting the 3x3 to 4x4.
 */
Matrix4x4 multiply(const Matrix3x3& left, const Matrix4x4& right);
Matrix4x4 multiply(const Matrix4x4& left, const Matrix3x3& right);

} // namespace render
