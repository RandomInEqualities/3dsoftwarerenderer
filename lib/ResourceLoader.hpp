#pragma once

#include "Model.hpp"
#include "Texture.hpp"

#include <map>
#include <string>
#include <vector>

namespace render
{

/**
 * Class for loading resources from files.
 */
class ResourceLoader
{
public:
    /**
     * Load the 3D model in a wavefront object file.
     *
     * If the triangles needs textures, they will also be loaded.
     *
     * Throws runtime_error if it can't load the model.
     */
    Model loadObjModel(const std::string& filename);

    /**
     * Load a texture in a file and return its loaded id.
     *
     * Throws runtime_error if it can't load the texture.
     */
    TextureId loadTgaTexture(const std::string& filename);

    /**
     * Get the texture corresponding to id.
     *
     * Throws out_of_range if no texture exists.
     */
    const Texture& getTexture(const TextureId id) const;

private:
    std::vector<Texture> m_textures;
    std::map<std::string, TextureId> m_textureNames;
};

} // namespace render
