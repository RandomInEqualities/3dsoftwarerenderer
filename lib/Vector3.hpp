#pragma once

#include "Assert.hpp"
#include "Math.hpp"

namespace render
{

/**
 * A 3-dimensional vector.
 */
template<typename T>
class Vector3
{
public:
    constexpr Vector3();
    constexpr Vector3(T X, T Y, T Z);

    /**
     * The length of the vector.
     */
    constexpr T length() const;

    /**
     * Normalize the vector have to a length of 1.
     */
    constexpr Vector3<T>& normalize();

    /**
     * Copy the vector and normalize the copy.
     */
    constexpr Vector3<T> normalized() const;

    /**
     * Addition and substraction of other vectors.
     */
    constexpr Vector3<T> operator-() const;
    constexpr Vector3<T> operator-(const Vector3<T>& right) const;
    constexpr Vector3<T> operator+(const Vector3<T>& right) const;
    constexpr Vector3<T>& operator+=(const Vector3<T>& right);
    constexpr Vector3<T>& operator-=(const Vector3<T>& right);

    /**
     * Multiplication with other vectors.
     */
    template<typename P, typename C>
    friend constexpr Vector3<P> operator*(const Vector3<P>& left, const C right);
    template<typename P, typename C>
    friend constexpr Vector3<P> operator*(const C left, const Vector3<P>& right);
    template<typename P, typename C>
    friend constexpr Vector3<P>& operator*=(Vector3<P>& left, const C right);

    /**
     * Division with other classes.
     */
    template<typename P, typename C>
    friend constexpr Vector3<P> operator/(const Vector3<P>& left, const C right);
    template<typename P, typename C>
    friend constexpr Vector3<P>& operator/=(Vector3<P>& left, const C right);

    T x{};
    T y{};
    T z{};
};

/**
 * Compute the dot product of two vectors.
 */
template<typename T>
constexpr T dotProduct(const Vector3<T>& a, const Vector3<T>& b);

/**
 * Compute the cross product of two vectors.
 */
template<typename T>
constexpr Vector3<T> crossProduct(const Vector3<T>& a, const Vector3<T>& b);

// Implementation Details.

template<typename T>
constexpr Vector3<T>::Vector3()
{
}

template<typename T>
constexpr Vector3<T>::Vector3(T X, T Y, T Z) : x(X), y(Y), z(Z)
{
}

template<typename T>
constexpr T Vector3<T>::length() const
{
    return math::sqrt(x * x + y * y + z * z);
}

template<typename T>
constexpr Vector3<T>& Vector3<T>::normalize()
{
    const T currentLength = length();
    if (math::isZero(currentLength))
    {
        RENDER_ASSERT(false, "Can't normalize zero vector");
        return *this;
    }
    if (!math::isEqual(currentLength, 1.0))
    {
        x /= currentLength;
        y /= currentLength;
        z /= currentLength;
    }
    return *this;
}

template<typename T>
constexpr Vector3<T> Vector3<T>::normalized() const
{
    Vector3<T> vector = *this;
    vector.normalize();
    return vector;
}

template<typename T>
constexpr Vector3<T> Vector3<T>::operator-() const
{
    return {-x, -y, -z};
}

template<typename T>
constexpr Vector3<T> Vector3<T>::operator-(const Vector3<T>& right) const
{
    return {x - right.x, y - right.y, z - right.z};
}

template<typename T>
constexpr Vector3<T> Vector3<T>::operator+(const Vector3<T>& right) const
{
    return {x + right.x, y + right.y, z + right.z};
}

template<typename T>
constexpr Vector3<T>& Vector3<T>::operator+=(const Vector3<T>& right)
{
    x += right.x;
    y += right.y;
    z += right.z;
    return *this;
}

template<typename T>
constexpr Vector3<T>& Vector3<T>::operator-=(const Vector3<T>& right)
{
    x -= right.x;
    y -= right.y;
    z -= right.z;
    return *this;
}

template<typename P, typename C>
constexpr Vector3<P> operator*(const Vector3<P>& left, const C right)
{
    return {left.x * right, left.y * right, left.z * right};
}

template<typename P, typename C>
constexpr Vector3<P> operator*(const C left, const Vector3<P>& right)
{
    return {left * right.x, left * right.y, left * right.z};
}

template<typename P, typename C>
constexpr Vector3<P> operator/(const Vector3<P>& left, const C right)
{
    return {left.x / right, left.y / right, left.z / right};
}

template<typename P, typename C>
constexpr Vector3<P>& operator/=(Vector3<P>& left, const C right)
{
    left.x /= right;
    left.y /= right;
    left.z /= right;
    return left;
}

template<typename T>
constexpr T dotProduct(const Vector3<T>& a, const Vector3<T>& b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

template<typename T>
constexpr Vector3<T> crossProduct(const Vector3<T>& a, const Vector3<T>& b)
{
    const T x = a.y * b.z - a.z * b.y;
    const T y = a.z * b.x - a.x * b.z;
    const T z = a.x * b.y - a.y * b.x;
    return {x, y, z};
}

} // namespace render
