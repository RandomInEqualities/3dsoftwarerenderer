#pragma once

#include "Matrix4x4.hpp"
#include "Time.hpp"
#include "Types.hpp"
#include "Vector3.hpp"

namespace render
{

/**
 * Camera class to represent a camera in a 3D scene.
 */
class Camera
{
public:
    explicit Camera();

    /**
     * Set the camera position, look at target and up direction.
     */
    void init(const Vector3<real64>& position, const Vector3<real64>& target, const Vector3<real64>& up);

    Vector3<real64> getPosition() const;
    void setPosition(const Vector3<real64>& position);

    Vector3<real64> getTarget() const;
    void setTarget(const Vector3<real64>& target);

    Vector3<real64> getUp() const;
    void setUp(const Vector3<real64>& up);

    /**
     * Get the matrix that transform from world space to camera space.
     */
    Matrix4x4 getViewTransform() const;

    /**
     * Modify the camera position and orientation. Can emulate a smoothly moving camera.
     *
     * Uses the set move speed and tilt speeds.
     */
    void moveForward(Seconds elapsedTime);
    void moveBackwards(Seconds elapsedTime);
    void strafeLeft(Seconds elapsedTime);
    void strafeRight(Seconds elapsedTime);
    void tiltRight(Seconds elapsedTime);
    void tiltLeft(Seconds elapsedTime);

    /**
     * Set the move speed in meters per second when using the move functions.
     */
    real64 getMoveSpeed() const;
    void setMoveSpeed(real64 MetersPerSecond);

    /**
     * Set the tilt speed in meters per second when using the tilt functions.
     */
    real64 getTiltSpeed() const;
    void setTiltSpeed(real64 MetersPerSecond);

    /**
     * Modify the camera position and orientation via mouse movement.
     */
    void mouseMoved(real64 deltaX, real64 deltaY);

private:
    /**
     * Tilt camera. Negative seconds tilts to the right. Positive to the left.
     */
    void tilt(Seconds elapsedTime);

    /**
     * Calculate the camera basis from the position, target and up direction.
     */
    void calculateCameraBasis();

    Vector3<real64> m_position;
    Vector3<real64> m_target;
    Vector3<real64> m_up;

    // The camera basis vectors.
    Vector3<real64> m_cameraBasisX;
    Vector3<real64> m_cameraBasisY;
    Vector3<real64> m_cameraBasisZ;

    real64 m_mouseSensitivityX{0.5};
    real64 m_mouseSensitivityY{0.5};

    // The move speed in meters/second.
    real64 m_moveSpeed{5.0};

    // The tilt speed in radians/second.
    real64 m_tiltSpeed{4.0};
};

} // namespace render
