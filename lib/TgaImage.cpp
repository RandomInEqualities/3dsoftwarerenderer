#include "TgaImage.hpp"
#include "Assert.hpp"

#include <bitset>
#include <fstream>
#include <limits>
#include <stdexcept>

namespace render::format::tga
{
namespace
{

struct TGAHeader
{
    // The ordering of this struct is the same as a tga file header.
    uint8 idLength = 0;
    uint8 mapType = 0;
    uint8 imageType = 0;
    uint16 mapIndexStart = 0;
    uint16 mapLength = 0;
    uint8 mapBitsPerColour = 0;
    uint16 xScreenOffset = 0;
    uint16 yScreenOffset = 0;
    uint16 width = 0;
    uint16 height = 0;
    uint8 bitsPerPixel = 0;
    uint8 descriptor = 0;
};

enum TGATypes : uint8
{
    NoImageData = 0,
    ColourMap = 1,
    ColourTrue = 2,
    ColourMono = 3,
    EncodedColourMap = 9,
    EncodedColourTrue = 10,
    EncodedColourMono = 11
};

[[maybe_unused]] void LogHeader(const TGAHeader& header, const std::string& title)
{
    std::string message = title + "\n";
    message += "idLength:         " + std::to_string(int32(header.idLength)) + "\n";
    message += "mapType:          " + std::to_string(int32(header.mapType)) + "\n";
    message += "imageType:        " + std::to_string(int32(header.imageType)) + "\n";
    message += "mapIndexStart:    " + std::to_string(int32(header.mapIndexStart)) + "\n";
    message += "mapLength:        " + std::to_string(int32(header.mapLength)) + "\n";
    message += "mapBitsPerColour: " + std::to_string(int32(header.mapBitsPerColour)) + "\n";
    message += "xScreenOffset:    " + std::to_string(int32(header.xScreenOffset)) + "\n";
    message += "ScreenOffset:     " + std::to_string(int32(header.yScreenOffset)) + "\n";
    message += "width:            " + std::to_string(int32(header.width)) + "\n";
    message += "height:           " + std::to_string(int32(header.height)) + "\n";
    message += "bitsPerPixel:     " + std::to_string(int32(header.bitsPerPixel)) + "\n";
    message += "descriptor:       " + std::bitset<8>(header.descriptor).to_string();
    log::log(message);
}

} // namespace

std::vector<uint8> loadImage(const std::string& filename, uint32& width, uint32& height)
{
    width = 0;
    height = 0;

    std::ifstream file(filename, std::ios::in | std::ios::binary);
    if (!file)
    {
        throw std::runtime_error(filename + " unable to open file.");
    }
    file.exceptions(std::ifstream::badbit | std::ifstream::failbit | std::ifstream::eofbit);

    // We read the TGA image. A format specification for TGA image files can be found found in
    // the doc folder.
    TGAHeader header;
    file.read(reinterpret_cast<char*>(&header.idLength), 1);
    file.read(reinterpret_cast<char*>(&header.mapType), 1);
    file.read(reinterpret_cast<char*>(&header.imageType), 1);
    file.read(reinterpret_cast<char*>(&header.mapIndexStart), 2);
    file.read(reinterpret_cast<char*>(&header.mapLength), 2);
    file.read(reinterpret_cast<char*>(&header.mapBitsPerColour), 1);
    file.read(reinterpret_cast<char*>(&header.xScreenOffset), 2);
    file.read(reinterpret_cast<char*>(&header.yScreenOffset), 2);
    file.read(reinterpret_cast<char*>(&header.width), 2);
    file.read(reinterpret_cast<char*>(&header.height), 2);
    file.read(reinterpret_cast<char*>(&header.bitsPerPixel), 1);
    file.read(reinterpret_cast<char*>(&header.descriptor), 1);

    // Check that we can load this specific tga image.
    if (header.imageType == TGATypes::NoImageData)
    {
        throw std::runtime_error(filename + " contains no image data.");
    }
    if (header.imageType != TGATypes::ColourTrue)
    {
        throw std::runtime_error(filename + " we only support true-colour tga images.");
    }
    if (header.bitsPerPixel != 24 && header.bitsPerPixel != 32)
    {
        throw std::runtime_error(filename + " we only support 24/32 bit images.");
    }

    // Skip the image description.
    file.seekg(header.idLength + file.tellg());

    // Skip the colour map.
    if (header.mapType != 0)
    {
        const std::streamoff bytes = header.mapBitsPerColour / 8 + ((header.mapBitsPerColour % 8 == 0) ? 0 : 1);
        const std::streamoff mapByteLength = bytes * header.mapLength;
        file.seekg(mapByteLength + file.tellg());
    }

    // Load the image.
    width = header.width;
    height = header.height;
    std::vector<uint8> rgba(4 * width * height);

    const bool origin_bot_left = (0B00110000 & header.descriptor) == 0B00000000;
    const bool origin_top_left = (0B00110000 & header.descriptor) == 0B00100000;
    const bool origin_bot_right = (0B00110000 & header.descriptor) == 0B00010000;
    const bool origin_top_right = (0B00110000 & header.descriptor) == 0B00110000;
    for (uint32 row = 0; row < height; row++)
    {
        for (uint32 col = 0; col < width; col++)
        {
            // The image that we return has origin in the top left corner. We translate the tga
            // image to this format.
            size_t index = 0;
            if (origin_top_left)
            {
                index = col + row * width;
            }
            else if (origin_bot_left)
            {
                index = width * height + col - (row + 1) * width;
            }
            else if (origin_bot_right)
            {
                index = width * height - col - 1 - row * width;
            }
            else if (origin_top_right)
            {
                index = (row + 1) * width - col;
            }

            // Read respectively the blue, green and red colour.
            file.read(reinterpret_cast<char*>(&rgba[4 * index + 2]), 1);
            file.read(reinterpret_cast<char*>(&rgba[4 * index + 1]), 1);
            file.read(reinterpret_cast<char*>(&rgba[4 * index + 0]), 1);

            // Read the alpha channel.
            if (header.bitsPerPixel == 32)
            {
                file.read(reinterpret_cast<char*>(&rgba[4 * index + 3]), 1);
            }
            else
            {
                rgba[4 * index + 3] = 255;
            }
        }
    }

    return rgba;
}

void saveImage(const std::string& filename, const std::vector<uint8>& image, const uint32 width, const uint32 height)
{
    RENDER_ASSERT(4 * width * height == image.size(), "Bad image dimension.");

    // Make sure that we can store the width and height in a tga image.
    const uint32 uint16Max = std::numeric_limits<uint16>::max();
    if (uint16Max < width || uint16Max < height)
    {
        throw std::runtime_error(filename + " width/height is larger than uint16.");
    }

    TGAHeader header;
    header.idLength = 0;
    header.mapType = 0;
    header.imageType = TGATypes::ColourTrue;
    header.mapIndexStart = 0;
    header.mapLength = 0;
    header.mapBitsPerColour = 0;
    header.xScreenOffset = 0;
    header.yScreenOffset = 0;
    header.width = static_cast<uint16>(width);
    header.height = static_cast<uint16>(height);
    header.bitsPerPixel = 32;

    // The image has an 8 bit alpha channel and its origin is the top left corner.
    header.descriptor = 0b00101000;

    std::ofstream file(filename, std::ios::out | std::ios::binary | std::ios::trunc);
    if (!file)
    {
        throw std::runtime_error(filename + " unable to save to file.");
    }
    file.exceptions(std::ifstream::badbit | std::ifstream::failbit);
    file.write(reinterpret_cast<const char*>(&header.idLength), 1);
    file.write(reinterpret_cast<const char*>(&header.mapType), 1);
    file.write(reinterpret_cast<const char*>(&header.imageType), 1);
    file.write(reinterpret_cast<const char*>(&header.mapIndexStart), 2);
    file.write(reinterpret_cast<const char*>(&header.mapLength), 2);
    file.write(reinterpret_cast<const char*>(&header.mapBitsPerColour), 1);
    file.write(reinterpret_cast<const char*>(&header.xScreenOffset), 2);
    file.write(reinterpret_cast<const char*>(&header.yScreenOffset), 2);
    file.write(reinterpret_cast<const char*>(&header.width), 2);
    file.write(reinterpret_cast<const char*>(&header.height), 2);
    file.write(reinterpret_cast<const char*>(&header.bitsPerPixel), 1);
    file.write(reinterpret_cast<const char*>(&header.descriptor), 1);
    file.write(reinterpret_cast<const char*>(image.data()), static_cast<std::streamsize>(image.size()));
}

} // namespace render::format::tga
