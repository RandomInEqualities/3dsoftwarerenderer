#include "WavefrontObjModel.hpp"
#include "StringUtils.hpp"

#include <charconv>
#include <fstream>
#include <ios>
#include <optional>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

namespace render::format::wavefront
{
namespace
{

/**
 * An object file face during the parsing phase. After the parsing phase we can convert it into
 * a Face without index locations.
 */
struct IndexFace
{
    // Index locations of the face vertices.
    std::vector<int32> vertices;

    // Optional index locations of texture positions and normals.
    std::vector<int32> texturePositions;
    std::vector<int32> normals;

    // Optional group and material.
    std::string group;
    std::string material;
};

/**
 * Convert a string to a Real
 */
std::optional<real64> stringToReal(const std::string& str)
{
    real64 value = 0.0;
    const auto result = std::from_chars(str.data(), str.data() + str.size(), value);
    if (result.ec == std::errc())
    {
        return value;
    }
    return std::nullopt;
}

/**
 * Convert a string to an integer.
 */
std::optional<int32> stringToInt(const std::string& str)
{
    int32 value = 0;
    const auto result = std::from_chars(str.data(), str.data() + str.size(), value);
    if (result.ec == std::errc())
    {
        return value;
    }
    return std::nullopt;
}

/**
 * Try to read a vertex string into a face structure. Throws logic_error on failure.
 */
void stringToVertex(std::string str, IndexFace& face)
{
    // An face string can have four forms v, v/vt, v/vt/vn and v//vn, where v = vertex,
    // vn = normal and vt = texture position.
    // ... read the vertex index.
    size_t readEnd = 0;
    face.vertices.emplace_back(std::stoi(str, &readEnd));
    if (readEnd == str.size())
    {
        return;
    }
    if (str.at(readEnd) != '/')
    {
        throw std::invalid_argument("Bad vertex.");
    }

    // ... read the texture position index.
    if (str.at(readEnd + 1) != '/')
    {
        str.erase(0, readEnd + 1);
        face.texturePositions.emplace_back(std::stoi(str, &readEnd));
        if (readEnd == str.size())
        {
            return;
        }
        if (str.at(readEnd) != '/')
        {
            throw std::invalid_argument("Bad vertex.");
        }
        str.erase(0, readEnd + 1);
    }
    else
    {
        str.erase(0, readEnd + 2);
    }

    // ... read the normal vector index.
    face.normals.emplace_back(std::stoi(str, &readEnd));
    if (readEnd != str.size())
    {
        throw std::invalid_argument("Bad vertex.");
    }
}

/**
 * The face needs to be balanced such that there are either a normal for each vertex or
 * zero normals. The same goes for the texture positions, zero or as many as there are vertices.
 * Throws logic_error if face is unbalanced.
 */
void checkFaceBalance(const IndexFace& face)
{
    const size_t vSize = face.vertices.size();
    const size_t vnSize = face.normals.size();
    const size_t vtSize = face.texturePositions.size();

    if (vnSize != 0 && vnSize != vSize)
    {
        throw std::logic_error("Unbalanced face");
    }
    if (vtSize != 0 && vtSize != vSize)
    {
        throw std::logic_error("Unbalanced face");
    }
}

/**
 * Function for easily throwing a runtime_error to signal a syntax error in the parsing of a file.
 */
void throwError(const std::string& filename, const int32 line)
{
    throw std::runtime_error(filename + " syntax error at line " + std::to_string(line));
}

} // namespace

void ObjModel::readFromFile(const std::string& filename)
{
    m_name.clear();
    m_faces.clear();
    m_materials.clear();

    std::ifstream file;
    file.open(filename);
    if (!file)
    {
        throw std::runtime_error(filename + " unable to open.");
    }
    file.exceptions(std::ifstream::badbit);

    // We use a subset of the object file format, see ObjModel.hpp for more info or
    // http://paulbourke.net/dataformats/obj/ for a full object file specification.

    // During file reading we load data into temporary structures that are converted to faces,
    // when everything has been read.
    std::vector<IndexFace> indexFaces;
    std::vector<Vector3<real64>> vertices;
    std::vector<Vector3<real64>> normals;
    std::vector<Vector2<real64>> texturePositions;

    // The currently active group and material.
    std::string group;
    std::string material;

    int32 lineNumber = 0;
    std::string line;
    while (std::getline(file, line))
    {
        lineNumber++;

        std::vector<std::string> words = splitIntoWords(line);
        if (words.empty())
        {
            continue;
        }
        words[0] = toLower(words[0]);

        if (words[0] == "v")
        {
            if (words.size() != 4 && words.size() != 5)
            {
                throwError(filename, lineNumber);
            }

            const std::optional<real64> x = stringToReal(words[1]);
            const std::optional<real64> y = stringToReal(words[2]);
            const std::optional<real64> z = stringToReal(words[3]);
            if (!x || !y || !z)
            {
                throwError(filename, lineNumber);
            }
            vertices.emplace_back(*x, *y, *z);
        }
        else if (words[0] == "vn")
        {
            if (words.size() != 4)
            {
                throwError(filename, lineNumber);
            }

            const std::optional<real64> x = stringToReal(words[1]);
            const std::optional<real64> y = stringToReal(words[2]);
            const std::optional<real64> z = stringToReal(words[3]);
            if (!x || !y || !z)
            {
                throwError(filename, lineNumber);
            }
            normals.emplace_back(*x, *y, *z);
        }
        else if (words[0] == "vt")
        {
            if (words.size() != 3 && words.size() != 4)
            {
                throwError(filename, lineNumber);
            }

            const std::optional<real64> x = stringToReal(words[1]);
            const std::optional<real64> y = stringToReal(words[2]);
            if (!x || !y)
            {
                throwError(filename, lineNumber);
            }
            texturePositions.emplace_back(*x, *y);
        }
        else if (words[0] == "f")
        {
            if (words.size() < 4)
            {
                throwError(filename, lineNumber);
            }
            IndexFace indexFace;
            try
            {
                for (size_t index = 1; index < words.size(); index++)
                {
                    stringToVertex(words[index], indexFace);
                }
                checkFaceBalance(indexFace);
            }
            catch (const std::logic_error&)
            {
                throwError(filename, lineNumber);
            }
            indexFace.group = group;
            indexFace.material = material;
            indexFaces.emplace_back(std::move(indexFace));
        }
        else if (words[0] == "mtllib")
        {
            for (size_t index = 1; index < words.size(); index++)
            {
                readMaterialFile(words[index]);
            }
        }
        else if (words[0] == "usemtl")
        {
            if (words.size() != 2)
            {
                throwError(filename, lineNumber);
            }
            material = words[1];
        }
        else if (words[0] == "g")
        {
            if (words.size() != 2)
            {
                throwError(filename, lineNumber);
            }
            group = words[1];
        }
        else if (words[0] == "o")
        {
            if (words.size() != 2)
            {
                throwError(filename, lineNumber);
            }
            m_name = words[1];
        }
    }

    // Check that each face has an empty or existing material.
    for (const IndexFace& face : indexFaces)
    {
        if (!face.material.empty())
        {
            try
            {
                getMaterial(face.material);
            }
            catch (const std::out_of_range&)
            {
                throw std::runtime_error(filename + " invalid material " + face.material + ".");
            }
        }
    }

    // Convert the index faces into faces.
    m_faces.reserve(indexFaces.size());
    for (size_t pos = 0; pos < indexFaces.size(); pos++)
    {
        const IndexFace& indexFace = indexFaces[pos];

        ObjFace face;
        try
        {
            for (const int32 index : indexFace.vertices)
            {
                const auto vertice = static_cast<size_t>(index - 1);
                face.vertices.emplace_back(vertices.at(vertice));
            }
            for (const int32 index : indexFace.normals)
            {
                const auto vertice = static_cast<size_t>(index - 1);
                face.normals.emplace_back(normals.at(vertice));
            }
            for (const int32 index : indexFace.texturePositions)
            {
                const auto vertice = static_cast<size_t>(index - 1);
                face.textures.emplace_back(texturePositions.at(vertice));
            }
        }
        catch (const std::out_of_range&)
        {
            const std::string message = " face " + std::to_string(pos) + " has invalid index.";
            throw std::runtime_error(filename + message);
        }

        face.group = indexFace.group;
        face.material = indexFace.material;
        m_faces.emplace_back(std::move(face));
    }
}

const ObjMaterial& ObjModel::getMaterial(const std::string& material) const
{
    for (const ObjMaterial& existing : m_materials)
    {
        if (material == existing.name)
        {
            return existing;
        }
    }
    throw std::out_of_range(m_name + " unable to find material " + material + ".");
}

const std::string& ObjModel::getName() const
{
    return m_name;
}

const std::vector<ObjFace>& ObjModel::getFaces() const
{
    return m_faces;
}

void ObjModel::addMaterial(const ObjMaterial& material)
{
    if (material.name.empty())
    {
        return;
    }

    // check that we do not have duplicates.
    for (ObjMaterial& existing : m_materials)
    {
        if (material.name == existing.name)
        {
            existing = material;
            return;
        }
    }

    m_materials.push_back(material);
}

void ObjModel::resetMaterial(ObjMaterial& material)
{
    material.name.clear();
    material.ambient = Vector3<real64>(0.0, 0.0, 0.0);
    material.diffuse = Vector3<real64>(0.0, 0.0, 0.0);
    material.specular = Vector3<real64>(0.0, 0.0, 0.0);
    material.transparency = 1.0;
    material.illumination = 0;
    material.ambientFilename.clear();
    material.diffuseFilename.clear();
    material.specularFilename.clear();
}

void ObjModel::readMaterialFile(const std::string& filename)
{
    std::ifstream file(filename);
    if (!file)
    {
        throw std::runtime_error(filename + " unable to open.");
    }
    file.exceptions(std::ifstream::badbit);

    // The mtl file format has a fairly simple specification. It is based on the Phong light
    // reflection model, but can easily be extended or used for other models. An mtl file
    // specification can be found at http://paulbourke.net/dataformats/mtl/.

    // The material that we are currently loading.
    ObjMaterial material;

    std::string line;
    int32 lineNumber = 0;
    while (std::getline(file, line))
    {
        lineNumber++;

        std::vector<std::string> words = splitIntoWords(line);
        if (words.empty())
        {
            continue;
        }
        words[0] = toLower(words[0]);

        if (words[0] == "newmtl")
        {
            if (words.size() != 2)
            {
                throwError(filename, lineNumber);
            }
            addMaterial(material);
            resetMaterial(material);
            material.name = words[1];
        }
        else if (words[0] == "ka" || words[0] == "kd" || words[0] == "ks")
        {
            if (words.size() != 4)
            {
                throwError(filename, lineNumber);
            }

            const std::optional<real64> x = stringToReal(words[1]);
            const std::optional<real64> y = stringToReal(words[2]);
            const std::optional<real64> z = stringToReal(words[3]);
            if (!x || !y || !z)
            {
                throwError(filename, lineNumber);
            }

            Vector3<real64> colour;
            colour.x = *x;
            colour.y = *y;
            colour.z = *z;

            if (words[0] == "ka")
            {
                material.ambient = colour;
            }
            else if (words[0] == "kd")
            {
                material.diffuse = colour;
            }
            else if (words[0] == "ks")
            {
                material.specular = colour;
            }
        }
        else if (words[0] == "tr" || words[0] == "d")
        {
            if (words.size() != 2)
            {
                throwError(filename, lineNumber);
            }

            if (const std::optional<real64> transparency = stringToReal(words[1]))
            {
                material.transparency = *transparency;
            }
            else
            {
                throwError(filename, lineNumber);
            }
        }
        else if (words[0] == "illum")
        {
            if (words.size() != 2)
            {
                throwError(filename, lineNumber);
            }

            if (const std::optional<int32> illumination = stringToInt(words[1]))
            {
                material.illumination = *illumination;
            }
            else
            {
                throwError(filename, lineNumber);
            }
        }
        else if (words[0] == "map_ka")
        {
            if (words.size() != 2)
            {
                throwError(filename, lineNumber);
            }
            material.ambientFilename = words[1];
        }
        else if (words[0] == "map_kd")
        {
            if (words.size() != 2)
            {
                throwError(filename, lineNumber);
            }
            material.diffuseFilename = words[1];
        }
        else if (words[0] == "map_ks")
        {
            if (words.size() != 2)
            {
                throwError(filename, lineNumber);
            }
            material.specularFilename = words[1];
        }
    }
    addMaterial(material);
}

} // namespace render::format::wavefront
