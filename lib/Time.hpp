#pragma once

#include "Types.hpp"

#include <chrono>
#include <ratio>

namespace render
{

/**
 * Clock that we use to measure time.
 */
using Clock = std::chrono::steady_clock;

/**
 * Representation of seconds and milliseconds.
 */
using Seconds = std::chrono::duration<real64>;
using MilliSeconds = std::chrono::duration<real64, std::milli>;

/**
 * A point in time.
 */
using TimePoint = std::chrono::time_point<Clock>;

/**
 * Allow using literals for time, for example 1.0s or 1.0ms.
 */
using namespace std::literals::chrono_literals;

} // namespace render
