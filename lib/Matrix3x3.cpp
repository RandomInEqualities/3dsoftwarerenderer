#include "Matrix3x3.hpp"
#include "Assert.hpp"
#include "Math.hpp"

namespace render
{

real64 Matrix3x3::computeDeterminant() const
{
    return m00 * (m11 * m22 - m21 * m12) - m01 * (m10 * m22 - m20 * m12) + m02 * (m10 * m21 - m20 * m11);
}

Matrix3x3 Matrix3x3::computeInverse() const
{
    const real64 determinant = computeDeterminant();
    if (math::isZero(determinant))
    {
        RENDER_ASSERT(false, "Singular matrix.");
        return *this;
    }
    const real64 oneOverDeterminant = 1.0 / determinant;

    Matrix3x3 inverse;
    inverse.m00 = (m11 * m22 - m12 * m21) * oneOverDeterminant;
    inverse.m01 = (m02 * m21 - m01 * m22) * oneOverDeterminant;
    inverse.m02 = (m01 * m12 - m02 * m11) * oneOverDeterminant;

    inverse.m10 = (m12 * m20 - m10 * m22) * oneOverDeterminant;
    inverse.m11 = (m00 * m22 - m02 * m20) * oneOverDeterminant;
    inverse.m12 = (m02 * m10 - m00 * m12) * oneOverDeterminant;

    inverse.m20 = (m10 * m21 - m11 * m20) * oneOverDeterminant;
    inverse.m21 = (m01 * m20 - m00 * m21) * oneOverDeterminant;
    inverse.m22 = (m00 * m11 - m01 * m10) * oneOverDeterminant;
    return inverse;
}

Matrix3x3& Matrix3x3::setupIdentity()
{
    m00 = 1.0;
    m01 = 0.0;
    m02 = 0.0;
    m10 = 0.0;
    m11 = 1.0;
    m12 = 0.0;
    m20 = 0.0;
    m21 = 0.0;
    m22 = 1.0;
    return *this;
}

Matrix3x3& Matrix3x3::setupRotation(Vector3<real64> axis, const real64 radians)
{
    const real64 sin = math::sin(radians);
    const real64 cos = math::cos(radians);
    const real64 oneMinusCos = 1.0 - cos;
    RENDER_ASSERT(!math::isZero(axis.length()), "Rotation axis is zero");
    axis.normalize();

    // From the standard rotation formula we compute the matrix elements.
    m00 = axis.x * axis.x * oneMinusCos + cos;
    m01 = axis.x * axis.y * oneMinusCos - axis.z * sin;
    m02 = axis.x * axis.z * oneMinusCos + axis.y * sin;

    m10 = axis.y * axis.x * oneMinusCos + axis.z * sin;
    m11 = axis.y * axis.y * oneMinusCos + cos;
    m12 = axis.y * axis.z * oneMinusCos - axis.x * sin;

    m20 = axis.z * axis.x * oneMinusCos - axis.y * sin;
    m21 = axis.z * axis.y * oneMinusCos + axis.x * sin;
    m22 = axis.z * axis.z * oneMinusCos + cos;
    return *this;
}

Matrix3x3& Matrix3x3::setupRotation(const Quaternion& q)
{
    // We use a derived formula to convert the quaternion to a rotation matrix.
    const real64 xx = 2.0 * q.x * q.x;
    const real64 yy = 2.0 * q.y * q.y;
    const real64 zz = 2.0 * q.z * q.z;

    m00 = 1.0 - yy - zz;
    m01 = 2.0 * q.x * q.y - 2.0 * q.w * q.z;
    m02 = 2.0 * q.x * q.z + 2.0 * q.w * q.y;

    m10 = 2.0 * q.x * q.y + 2.0 * q.w * q.z;
    m11 = 1.0 - xx - zz;
    m12 = 2.0 * q.y * q.z - 2.0 * q.w * q.x;

    m20 = 2.0 * q.x * q.z - 2.0 * q.w * q.y;
    m21 = 2.0 * q.y * q.z + 2.0 * q.w * q.x;
    m22 = 1.0 - xx - yy;
    return *this;
}

Matrix3x3& Matrix3x3::setupScale(Vector3<real64> axis, const real64 scale)
{
    RENDER_ASSERT(!math::isZero(axis.length()), "Scale axis is zero");
    axis.normalize();
    const real64 scaleMinusOne = scale - 1.0;

    m00 = 1.0 + scaleMinusOne * axis.x * axis.x;
    m01 = scaleMinusOne * axis.x * axis.y;
    m02 = scaleMinusOne * axis.x * axis.z;

    m10 = scaleMinusOne * axis.x * axis.y;
    m11 = 1.0 + scaleMinusOne * axis.y * axis.y;
    m12 = scaleMinusOne * axis.y * axis.z;

    m20 = scaleMinusOne * axis.x * axis.z;
    m21 = scaleMinusOne * axis.y * axis.z;
    m22 = 1.0 + scaleMinusOne * axis.z * axis.z;
    return *this;
}

Matrix4x4 Matrix3x3::as4x4()
{
    Matrix4x4 result;
    result.m00 = m00;
    result.m01 = m01;
    result.m02 = m02;
    result.tx = 0.0;
    result.m10 = m10;
    result.m11 = m11;
    result.m12 = m12;
    result.ty = 0.0;
    result.m20 = m20;
    result.m21 = m21;
    result.m22 = m22;
    result.tz = 0.0;
    result.m30 = 0.0;
    result.m31 = 0.0;
    result.m32 = 0.0;
    result.tw = 1.0;
    return result;
}

Matrix3x3 operator*(const Matrix3x3& a, const Matrix3x3& b)
{
    Matrix3x3 result;
    result.m00 = a.m00 * b.m00 + a.m01 * b.m10 + a.m02 * b.m20;
    result.m01 = a.m00 * b.m01 + a.m01 * b.m11 + a.m02 * b.m21;
    result.m02 = a.m00 * b.m02 + a.m01 * b.m12 + a.m02 * b.m22;

    result.m10 = a.m10 * b.m00 + a.m11 * b.m10 + a.m12 * b.m20;
    result.m11 = a.m10 * b.m01 + a.m11 * b.m11 + a.m12 * b.m21;
    result.m12 = a.m10 * b.m02 + a.m11 * b.m12 + a.m12 * b.m22;

    result.m20 = a.m20 * b.m00 + a.m21 * b.m10 + a.m22 * b.m20;
    result.m21 = a.m20 * b.m01 + a.m21 * b.m11 + a.m22 * b.m21;
    result.m22 = a.m20 * b.m02 + a.m21 * b.m12 + a.m22 * b.m22;
    return result;
}

Matrix3x3& operator*=(Matrix3x3& left, const Matrix3x3& right)
{
    left = right * left;
    return left;
}

Vector3<real64> operator*(const Matrix3x3& a, const Vector3<real64>& b)
{
    Vector3<real64> result;
    result.x = a.m00 * b.x + a.m01 * b.y + a.m02 * b.z;
    result.y = a.m10 * b.x + a.m11 * b.y + a.m12 * b.z;
    result.z = a.m20 * b.x + a.m21 * b.y + a.m22 * b.z;
    return result;
}

Vector3<real64>& operator*=(Vector3<real64>& left, const Matrix3x3& right)
{
    left = right * left;
    return left;
}

Matrix4x4 multiply(const Matrix3x3& a, const Matrix4x4& b)
{
    Matrix4x4 result;
    result.m00 = a.m00 * b.m00 + a.m01 * b.m10 + a.m02 * b.m20;
    result.m01 = a.m00 * b.m01 + a.m01 * b.m11 + a.m02 * b.m21;
    result.m02 = a.m00 * b.m02 + a.m01 * b.m12 + a.m02 * b.m22;
    result.tx = a.m00 * b.tx + a.m01 * b.ty + a.m02 * b.tz;

    result.m10 = a.m10 * b.m00 + a.m11 * b.m10 + a.m12 * b.m20;
    result.m11 = a.m10 * b.m01 + a.m11 * b.m11 + a.m12 * b.m21;
    result.m12 = a.m10 * b.m02 + a.m11 * b.m12 + a.m12 * b.m22;
    result.ty = a.m10 * b.tx + a.m11 * b.ty + a.m12 * b.tz;

    result.m20 = a.m20 * b.m00 + a.m21 * b.m10 + a.m22 * b.m20;
    result.m21 = a.m20 * b.m01 + a.m21 * b.m11 + a.m22 * b.m21;
    result.m22 = a.m20 * b.m02 + a.m21 * b.m12 + a.m22 * b.m22;
    result.tz = a.m20 * b.tx + a.m21 * b.ty + a.m22 * b.tz;

    result.m30 = b.m30;
    result.m31 = b.m31;
    result.m32 = b.m32;
    result.tw = b.tw;
    return result;
}

Matrix4x4 multiply(const Matrix4x4& a, const Matrix3x3& b)
{
    Matrix4x4 result;
    result.m00 = a.m00 * b.m00 + a.m01 * b.m10 + a.m02 * b.m20;
    result.m01 = a.m00 * b.m01 + a.m01 * b.m11 + a.m02 * b.m21;
    result.m02 = a.m00 * b.m02 + a.m01 * b.m12 + a.m02 * b.m22;
    result.tx = a.tx;

    result.m10 = a.m10 * b.m00 + a.m11 * b.m10 + a.m12 * b.m20;
    result.m11 = a.m10 * b.m01 + a.m11 * b.m11 + a.m12 * b.m21;
    result.m12 = a.m10 * b.m02 + a.m11 * b.m12 + a.m12 * b.m22;
    result.ty = a.ty;

    result.m20 = a.m20 * b.m00 + a.m21 * b.m10 + a.m22 * b.m20;
    result.m21 = a.m20 * b.m01 + a.m21 * b.m11 + a.m22 * b.m21;
    result.m22 = a.m20 * b.m02 + a.m21 * b.m12 + a.m22 * b.m22;
    result.tz = a.tz;

    result.m30 = a.m30 * b.m00 + a.m31 * b.m10 + a.m32 * b.m20;
    result.m31 = a.m30 * b.m01 + a.m31 * b.m11 + a.m32 * b.m21;
    result.m32 = a.m30 * b.m02 + a.m31 * b.m12 + a.m32 * b.m22;
    result.tw = a.tw;
    return result;
}

} // namespace render
