#pragma once

#include "Types.hpp"

#include <cmath>
#include <limits>

namespace render::math
{

/**
 * Get the value of pi.
 */
constexpr real64 pi{3.141592653589793};

/**
 * Threshold for when to assume that a number is zero.
 */
constexpr real64 zeroTolerance{1e-10};

/**
 * The lowest representable number in the given real.
 */
template<typename Real>
constexpr Real lowest()
{
    return std::numeric_limits<Real>::lowest();
}

/**
 * The highest representable number in the given real.
 */
template<typename Real>
constexpr Real highest()
{
    return std::numeric_limits<Real>::max();
}

/**
 * Test if a real number is zero. Uses Real's zero tolerance constant.
 */
template<typename Real>
constexpr bool isZero(const Real x)
{
    return std::abs(x) < zeroTolerance;
}

/**
 * Tests if two real numbers are equal. Uses Real's zero tolerance constant.
 */
template<typename Real>
constexpr bool isEqual(const Real x, const Real y)
{
    return std::abs(x - y) < zeroTolerance;
}

/**
 * Convert radians to degress.
 */
template<typename Real>
constexpr Real radToDeg(const Real radian)
{
    return 180.0 * radian / pi;
}

/**
 * Convert degrees to radians.
 */
template<typename Real>
constexpr Real degToRad(const Real degree)
{
    return pi * degree / 180.0;
}

/**
 * Get the square root of the given number.
 */
inline real64 sqrt(const real64 x)
{
    return std::sqrt(x);
}

/**
 * Round to nearest whole number.
 */
inline real64 round(const real64 x)
{
    return std::round(x);
}

/**
 * Compute sine trigonometric function value. This accepts radians.
 */
inline real64 sin(const real64 radian)
{
    return std::sin(radian);
}

/**
 * Compute cosine trigonometric function value. This accepts radians.
 */
inline real64 cos(const real64 radian)
{
    return std::cos(radian);
}

/**
 * Compute tangens trigonometric function value. This accepts radians.
 */
inline real64 tan(const real64 radian)
{
    return std::tan(radian);
}

/**
 * Inverse sine trigonometric function. If x is outside [-1,1] we clamp x into the interval.
 */
inline real64 asin(const real64 x)
{
    if (x <= -1.0)
    {
        return -pi / 2.0;
    }
    if (x >= 1.0)
    {
        return pi / 2.0;
    }
    // We can now use std::asin().
    return std::asin(x);
}

/**
 * Inverse consine trigonometric function. If x is outside [-1,1] we clamp x into the interval.
 */
inline real64 acos(const real64 x)
{
    if (x <= -1.0)
    {
        return pi;
    }
    if (x >= 1.0)
    {
        return 0.0;
    }
    // We can now use std::acos().
    return std::acos(x);
}

/**
 * Inverse tangens trigonometric function.
 */
inline real64 atan(const real64 x)
{
    return std::atan(x);
}

} // namespace render::math
