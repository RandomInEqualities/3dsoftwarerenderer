#include "Entity.hpp"
#include "Math.hpp"

namespace render
{

Entity::Entity(const Entity& entity)
    : m_model(entity.m_model),
      m_position(entity.m_position),
      m_rotation(entity.m_rotation),
      m_currentMove(entity.m_currentMove),
      m_currentRotation(entity.m_currentRotation),
      m_instructionsEnabled(entity.m_instructionsEnabled)
{
    for (const auto& instruction : entity.m_moveInstructions)
    {
        m_moveInstructions.push_back(instruction->clone());
    }
    for (const auto& instruction : entity.m_rotateInstructions)
    {
        m_rotateInstructions.push_back(instruction->clone());
    }
}

Entity& Entity::operator=(const Entity& entity)
{
    if (this != &entity)
    {
        m_model = entity.m_model;
        m_position = entity.m_position;
        m_rotation = entity.m_rotation;
        m_currentMove = entity.m_currentMove;
        m_currentRotation = entity.m_currentRotation;
        m_instructionsEnabled = entity.m_instructionsEnabled;

        m_moveInstructions.clear();
        for (const auto& instruction : entity.m_moveInstructions)
        {
            m_moveInstructions.push_back(instruction->clone());
        }

        m_rotateInstructions.clear();
        for (const auto& instruction : entity.m_rotateInstructions)
        {
            m_rotateInstructions.push_back(instruction->clone());
        }
    }
    return *this;
}

const Model& Entity::getModel() const
{
    return m_model;
}

void Entity::setModel(const Model& model)
{
    m_model = model;
}

Vector3<real64> Entity::getPosition() const
{
    return m_position;
}

void Entity::setPosition(const Vector3<real64>& position)
{
    m_position = position;
}

Quaternion Entity::getRotation() const
{
    return m_rotation;
}

void Entity::setRotation(const Quaternion& rotation)
{
    m_rotation = rotation;
}

void Entity::addMoveInstruction(std::unique_ptr<Instruction> instruction)
{
    m_moveInstructions.push_back(std::move(instruction));
}

void Entity::addRotateInstruction(std::unique_ptr<Instruction> instruction)
{
    m_rotateInstructions.push_back(std::move(instruction));
}

void Entity::clearMoveInstructions()
{
    m_moveInstructions.clear();
    m_currentMove = 0;
}

void Entity::clearRotationInstructions()
{
    m_rotateInstructions.clear();
    m_currentRotation = 0;
}

bool Entity::isInstructionsEnabled() const
{
    return m_instructionsEnabled;
}

void Entity::setInstructionsEnabled(const bool enabled)
{
    m_instructionsEnabled = enabled;
}

void Entity::toggleInstructionsEnabled()
{
    m_instructionsEnabled = !m_instructionsEnabled;
}

void Entity::update(const Seconds elapsed)
{
    if (!isInstructionsEnabled())
    {
        return;
    }

    if (m_currentMove < m_moveInstructions.size())
    {
        const auto& Instruction = m_moveInstructions[m_currentMove];
        Instruction->updateEntity(*this, elapsed);
        m_currentMove = Instruction->nextInstruction(m_currentMove);
    }

    if (m_currentRotation < m_rotateInstructions.size())
    {
        const auto& Instruction = m_rotateInstructions[m_currentRotation];
        Instruction->updateEntity(*this, elapsed);
        m_currentRotation = Instruction->nextInstruction(m_currentRotation);
    }
}

void Entity::setupRotationInLocalSpace(Entity& entity, const Vector3<real64> axis, const real64 speed)
{
    Quaternion rotation1;
    rotation1.setupRotation(axis, 0);
    entity.addRotateInstruction(std::make_unique<RotateInstruction>(rotation1, speed));

    Quaternion rotation2;
    rotation2.setupRotation(axis, 2 * math::pi / 3);
    entity.addRotateInstruction(std::make_unique<RotateInstruction>(rotation2, speed));

    Quaternion rotation3;
    rotation3.setupRotation(axis, 4 * math::pi / 3);
    entity.addRotateInstruction(std::make_unique<RotateInstruction>(rotation3, speed));

    entity.addRotateInstruction(std::make_unique<RepeatInstruction>(0));

    entity.setInstructionsEnabled(true);
}

MoveInstruction::MoveInstruction(const Vector3<real64> pos, const real64 speed)
{
    m_position = pos;
    m_moveSpeed = speed;
    m_reachedEnd = false;
}

void MoveInstruction::updateEntity(Entity& entity, const Seconds elapsed)
{
    Vector3<real64> moveVector = m_position - entity.getPosition();
    const real64 distance = moveVector.length();
    const real64 moveDistance = m_moveSpeed * elapsed.count();

    if (moveDistance < distance)
    {
        moveVector.normalize();
        const Vector3<real64> NewPosition = entity.getPosition() + moveVector * m_moveSpeed * elapsed.count();
        entity.setPosition(NewPosition);
    }
    else
    {
        entity.setPosition(m_position);
        m_reachedEnd = true;
    }
}

size_t MoveInstruction::nextInstruction(const size_t currentInstruction)
{
    if (m_reachedEnd)
    {
        m_reachedEnd = false;
        return currentInstruction + 1;
    }
    return currentInstruction;
}

std::unique_ptr<Instruction> MoveInstruction::clone() const
{
    return std::make_unique<MoveInstruction>(*this);
}

RotateInstruction::RotateInstruction()
{
    m_rotation.setupIdentity();
    m_angularSpeed = math::pi / 10.0;
    m_reachedEnd = false;
}

RotateInstruction::RotateInstruction(const Quaternion rotation, const real64 angularSpeed)
{
    m_rotation = rotation;
    m_angularSpeed = angularSpeed;
    m_reachedEnd = false;
}

void RotateInstruction::updateEntity(Entity& entity, const Seconds elapsed)
{
    const real64 angle = 2 * math::acos(dotProduct(entity.getRotation(), m_rotation));
    const real64 moveAngle = m_angularSpeed * elapsed.count();

    if (moveAngle < angle && !math::isZero(angle))
    {
        const Quaternion NewRotation = slerp(entity.getRotation(), m_rotation, moveAngle / angle);
        entity.setRotation(NewRotation);
    }
    else
    {
        entity.setRotation(m_rotation);
        m_reachedEnd = true;
    }
}

size_t RotateInstruction::nextInstruction(const size_t currentInstruction)
{
    if (m_reachedEnd)
    {
        m_reachedEnd = false;
        return currentInstruction + 1;
    }
    return currentInstruction;
}

std::unique_ptr<Instruction> RotateInstruction::clone() const
{
    return std::make_unique<RotateInstruction>(*this);
}

WaitInstruction::WaitInstruction(const Seconds waitTime) : m_remainingWaitTime{waitTime}
{
}

void WaitInstruction::updateEntity(Entity&, const Seconds elapsed)
{
    m_remainingWaitTime -= elapsed;
}

size_t WaitInstruction::nextInstruction(const size_t currentInstruction)
{
    if (m_remainingWaitTime <= 0.0s)
    {
        return currentInstruction + 1;
    }
    return currentInstruction;
}

std::unique_ptr<Instruction> WaitInstruction::clone() const
{
    return std::make_unique<WaitInstruction>(*this);
}

RepeatInstruction::RepeatInstruction(const size_t repeatIndex)
{
    m_repeatIndex = repeatIndex;
}

void RepeatInstruction::updateEntity(Entity&, Seconds)
{
}

size_t RepeatInstruction::nextInstruction(size_t)
{
    return m_repeatIndex;
}

std::unique_ptr<Instruction> RepeatInstruction::clone() const
{
    return std::make_unique<RepeatInstruction>(*this);
}

void StopInstruction::updateEntity(Entity& entity, Seconds)
{
    entity.setInstructionsEnabled(false);
}

size_t StopInstruction::nextInstruction(size_t)
{
    return 0;
}

std::unique_ptr<Instruction> StopInstruction::clone() const
{
    return std::make_unique<StopInstruction>(*this);
}

} // namespace render
