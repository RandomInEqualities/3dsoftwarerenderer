#
# About
#

A 3D software rasterisation renderer. It can load arbitrary .obj files and display its 3D content,
using nothing but the CPU to render images. There are no external dependencies in the rendering
code. It uses SFML for easy display of rendered images on the screen.

#
# Requires
#

Building the project requires CMake, C++17 compiler and SFML (http://www.sfml-dev.org/).
