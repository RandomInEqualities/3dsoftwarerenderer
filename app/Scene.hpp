#pragma once

#include "Camera.hpp"
#include "Entity.hpp"
#include "Model.hpp"
#include "Renderer.hpp"
#include "Time.hpp"
#include "Types.hpp"
#include "Vector2.hpp"

#include <SFML/Graphics.hpp>

#include <map>
#include <vector>

namespace render::scene
{

/**
 * Window that displays a 3D scene. Controls window display, user input and rendering.
 *
 * Scene handles a camera, entities (models, objects), rendering of entities, displaying the
 * rendered images and handling user input events.
 */
class Scene
{
public:
    explicit Scene();

    /**
     * Begin the scene event loop. Returns when user exits the scene window.
     */
    void begin();

private:
    enum class FovType
    {
        Horizontal,
        Vertical,
    };

    /**
     * Set the window and renderer resolution. Recreates the window if it is open.
     */
    void setResolution(Vector2<int32> resolution);

    /**
     * Set field of view. The one not specified is computed to match the monitor resolution.
     */
    void setFov(FovType type, real64 degrees);

    void createWindow();
    void moveMouseToCenter();
    void loadEntityFromObj(const std::string& filename);
    void initializeFpsText();
    void updateFpsText(Seconds elapsedTime);

    void processEvents();
    void handleLostFocus();
    void handleKeyPress(const sf::Event& event);
    void handleKeyRelease(const sf::Event& event);
    void handleMouseMove(const sf::Event& event);

    void updateCameraPosition(Seconds elapsedTime);
    void updateEntities(Seconds elapsedTime);

    // The rendering and display of an image of the scene works by:
    // 1) The camera view transform, window resolution and zoom is passed to the renderer.
    // 2) The renderer render the entities in the scene into a frame buffer.
    // 3) We convert the frame buffer to a graphics card texture, which the window displays.
    Camera m_camera;
    std::vector<Entity> m_entities;
    ResourceLoader m_loader;
    Renderer m_renderer;

    TimePoint m_frameStartTime;
    Vector2<int32> m_resolution{800, 800};
    Vector2<real64> m_fov{70.0, 70.0};
    bool m_windowVsync = false;

    sf::RenderWindow m_window;
    sf::Uint32 m_windowStyle = sf::Style::Default;
    sf::Texture m_windowTexture;
    sf::Sprite m_windowSprite;
    std::map<sf::Keyboard::Key, bool> m_keyIsPressed;

    sf::Text m_fpsText;
    sf::Font m_fpsFont;
    Seconds m_fpsTextUpdateCounter{0s};
    int32 m_fpsTextFrames = 0;
};

}; // namespace render::scene
