#include "Scene.hpp"
#include "Log.hpp"
#include "Math.hpp"
#include "Vector3.hpp"

#include <SFML/Graphics.hpp>

#include <memory>
#include <stdexcept>
#include <string>

namespace render::scene
{

Scene::Scene() : m_renderer(m_loader)
{
    // Create scene entities and load their resources.
    loadEntityFromObj("data/geometry/teapot.obj");
    loadEntityFromObj("data/geometry/cube.obj");
    loadEntityFromObj("data/geometry/humanoid_tri.obj");
    loadEntityFromObj("data/geometry/skull.obj");
    loadEntityFromObj("data/geometry/bunny.obj");

    // Setup some predefined positions for the objects.
    m_entities.at(0).setPosition({100, 100, -40});
    m_entities.at(0).setRotation(Quaternion{{0, 0, 1}, 0});
    Entity::setupRotationInLocalSpace(m_entities.at(0), {0, 0, 1}, 1.5);
    m_entities.at(1).setPosition({0, 4, 0});
    m_entities.at(2).setPosition({10, 10, 0});
    m_entities.at(2).setRotation(Quaternion{{1, 0, 0}, math::pi / 4.0});
    m_entities.at(3).setPosition({15, 15, 0});

    // Set initial camera position.
    const Vector3<real64> cameraPosition{-0.2, -0.2, 0.2};
    const Vector3<real64> cameraTarget{0, 0, 0.2};
    const Vector3<real64> cameraUp{0, 0, 1};
    m_camera.init(cameraPosition, cameraTarget, cameraUp);

    // Update the window texture, window sprite, renderer resolution and renderer fov.
    setResolution(m_resolution);
}

void Scene::begin()
{
    initializeFpsText();
    createWindow();

    m_frameStartTime = Clock::now();

    while (m_window.isOpen())
    {
        const TimePoint lastFrameStartTime = m_frameStartTime;
        m_frameStartTime = Clock::now();
        const MilliSeconds elapsedTime = (m_frameStartTime - lastFrameStartTime);

        updateFpsText(elapsedTime);
        processEvents();
        updateCameraPosition(elapsedTime);
        updateEntities(elapsedTime);

        m_renderer.setViewTransform(m_camera.getViewTransform());
        m_renderer.render(m_entities);
        m_windowTexture.update(m_renderer.getFrameBuffer().data());

        m_window.clear();
        m_window.draw(m_windowSprite);
        m_window.draw(m_fpsText);
        m_window.display();
    }
}

void Scene::createWindow()
{
    const sf::VideoMode mode(m_resolution.x, m_resolution.y, 32);
    m_window.create(mode, "3D Software Renderer", m_windowStyle);
    m_window.setMouseCursorVisible(false);
    m_window.setKeyRepeatEnabled(false);
    m_window.setVerticalSyncEnabled(m_windowVsync);
    moveMouseToCenter();
}

void Scene::moveMouseToCenter()
{
    if (m_window.hasFocus())
    {
        sf::Vector2i centre;
        centre.x = m_resolution.x / 2;
        centre.y = m_resolution.y / 2;
        sf::Mouse::setPosition(centre, m_window);
    }
}

void Scene::loadEntityFromObj(const std::string& filename)
{
    try
    {
        Entity entity;
        entity.setModel(m_loader.loadObjModel(filename));
        m_entities.push_back(std::move(entity));
    }
    catch (const std::runtime_error& error)
    {
        log::log(error.what());
    }
}

void Scene::initializeFpsText()
{
    m_fpsText.setFillColor(sf::Color::Yellow);
    m_fpsText.setOutlineColor(sf::Color::Red);
    m_fpsText.setString("FPS: ...");

    m_fpsFont.loadFromFile("data/font/sansation.ttf");
    m_fpsText.setFont(m_fpsFont);

    m_fpsTextUpdateCounter = 0s;
    m_fpsTextFrames = 0;
}

void Scene::updateFpsText(const Seconds elapsedTime)
{
    m_fpsTextUpdateCounter += elapsedTime;

    if (m_fpsTextUpdateCounter >= 1s)
    {
        m_fpsText.setString(" FPS: " + std::to_string(m_fpsTextFrames));
        m_fpsTextUpdateCounter = 0s;
        m_fpsTextFrames = 0;
    }
    else
    {
        m_fpsTextFrames++;
    }
}

void Scene::setResolution(const Vector2<int32> resolution)
{
    m_resolution = resolution;
    m_windowTexture.create(resolution.x, resolution.y);
    m_windowSprite.setTexture(m_windowTexture, true);
    m_renderer.setResolution(resolution);

    // Update the vertical fov to match the new resolution.
    setFov(FovType::Horizontal, m_fov.x);

    // Recreate window if it is open.
    if (m_window.isOpen())
    {
        createWindow();
    }
}

void Scene::setFov(const FovType type, const real64 degrees)
{
    // Physical monitor set-up, used to compute proper pixel aspect ratios for FOV and zoom values.
    // These should ideally be retrieved from the system.
    const Vector2<uint32> monitorAspect{16, 9};
    const Vector2<uint32> monitorResolution{1920, 1080};

    const real64 monitorAspectFrac = real64(monitorAspect.x) / real64(monitorAspect.y);
    const real64 monitorResolutionFracInv = real64(monitorResolution.y) / real64(monitorResolution.x);
    const real64 windowResolutionFrac = real64(m_resolution.x) / real64(m_resolution.y);

    // We compute the zoom values along side the fov. The zoom is passed to the renderer.
    Vector2<real64> zoom;
    if (type == FovType::Horizontal)
    {
        // Compute horizontal zoom/fov and adjust vertical to fit window.
        m_fov.x = degrees;
        zoom.x = 1.0 / math::tan(math::degToRad(degrees) / 2.0);
        zoom.y = zoom.x * windowResolutionFrac * monitorAspectFrac * monitorResolutionFracInv;
        m_fov.y = 2.0 * math::radToDeg(math::atan(1.0 / zoom.y));
    }
    else
    {
        // Compute vertical zoom and adjust horizontal to fit window.
        m_fov.y = degrees;
        zoom.y = 1.0 / math::tan(math::degToRad(degrees) / 2.0);
        zoom.x = zoom.y / (windowResolutionFrac * monitorAspectFrac * monitorResolutionFracInv);
        m_fov.x = 2.0 * math::radToDeg(math::atan(1.0 / zoom.x));
    }
    m_renderer.setZoom(zoom);
}

void Scene::updateCameraPosition(const Seconds elapsedTime)
{
    if (m_keyIsPressed[sf::Keyboard::W])
    {
        m_camera.moveForward(elapsedTime);
    }
    if (m_keyIsPressed[sf::Keyboard::S])
    {
        m_camera.moveBackwards(elapsedTime);
    }
    if (m_keyIsPressed[sf::Keyboard::D])
    {
        m_camera.strafeRight(elapsedTime);
    }
    if (m_keyIsPressed[sf::Keyboard::A])
    {
        m_camera.strafeLeft(elapsedTime);
    }
    if (m_keyIsPressed[sf::Keyboard::E])
    {
        m_camera.tiltRight(elapsedTime);
    }
    if (m_keyIsPressed[sf::Keyboard::Q])
    {
        m_camera.tiltLeft(elapsedTime);
    }
}

void Scene::updateEntities(const Seconds elapsedTime)
{
    for (Entity& entity : m_entities)
    {
        if (entity.isInstructionsEnabled())
        {
            entity.update(elapsedTime);
        }
    }
}

void Scene::processEvents()
{
    // Retrieve all window events that have happened since last ProcessEvents call.
    sf::Event event{};
    while (m_window.pollEvent(event))
    {
        switch (event.type)
        {
        case sf::Event::LostFocus:
            handleLostFocus();
            break;
        case sf::Event::KeyPressed:
            handleKeyPress(event);
            break;
        case sf::Event::KeyReleased:
            handleKeyRelease(event);
            break;
        case sf::Event::MouseMoved:
            handleMouseMove(event);
            break;
        case sf::Event::Closed:
            m_window.close();
            break;
        default:
            break;
        }
    }

    // Reset the mouse position to the window centre.
    moveMouseToCenter();
}

void Scene::handleLostFocus()
{
    // Do nothing until focus is regained.
    sf::Event event{};
    do
    {
        m_window.waitEvent(event);
    } while (event.type != sf::Event::GainedFocus);

    // Reset the keyboard presses.
    m_keyIsPressed.clear();

    // Reset the frame time.
    m_frameStartTime = Clock::now();
}

void Scene::handleKeyPress(const sf::Event& event)
{
    // Update the keyboard state.
    m_keyIsPressed[event.key.code] = true;

    // Process any key press triggers.
    switch (event.key.code)
    {
    case sf::Keyboard::G:
        m_renderer.setDrawWireframes(!m_renderer.isDrawingWireframes());
        break;
    case sf::Keyboard::H:
        m_renderer.setDrawColours(!m_renderer.isDrawingColours());
        break;
    case sf::Keyboard::Y:
        m_windowVsync = !m_windowVsync;
        m_window.setVerticalSyncEnabled(m_windowVsync);
        break;
    case sf::Keyboard::R:
        if (m_windowStyle != sf::Style::Fullscreen)
        {
            m_windowStyle = sf::Style::Fullscreen;
            setResolution({1920, 1080});
        }
        else
        {
            m_windowStyle = sf::Style::Default;
            setResolution({800, 800});
        }
        break;
    case sf::Keyboard::T:
        if (math::isEqual(m_fov.x, 120.0))
        {
            setFov(FovType::Horizontal, 90.0);
        }
        else
        {
            setFov(FovType::Horizontal, 120.0);
        }
        break;
    case sf::Keyboard::Num1:
        m_entities.at(0).toggleInstructionsEnabled();
        break;
    case sf::Keyboard::Num2:
        if (m_camera.getMoveSpeed() > 1.0)
        {
            m_camera.setMoveSpeed(0.1);
        }
        else
        {
            m_camera.setMoveSpeed(5.0);
        }
        break;
    case sf::Keyboard::Escape:
        m_window.close();
        break;
    default:
        break;
    }
}

void Scene::handleKeyRelease(const sf::Event& event)
{
    m_keyIsPressed[event.key.code] = false;
}

void Scene::handleMouseMove(const sf::Event& event)
{
    const Vector2<int32> halfResolution = m_resolution / 2;
    const real64 deltaX = real64(event.mouseMove.x - halfResolution.x) / real64(m_resolution.x);
    const real64 deltaY = real64(event.mouseMove.y - halfResolution.y) / real64(m_resolution.y);
    m_camera.mouseMoved(deltaX, deltaY);
}

} // namespace render::scene
